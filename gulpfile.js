var gulp 				= require('gulp-v4'),
	sass 					= require('gulp-sass'),
	babel 				= require('gulp-babel'),
	concat 				= require('gulp-concat'),
	uglify 				= require('gulp-uglify'),
	browserSync 	= require('browser-sync').create(),
	cleanCSS 			= require('gulp-clean-css'),
	cssnano 			= require("cssnano"),
	postcss 			= require("gulp-postcss"),
	autoprefixer 	= require("autoprefixer"),
	plumber 			= require('gulp-plumber'),
	del 					= require('del');

var wp_theme_name 	= 'tq/';
var broser_sync_proxy 	= 'local.tq';
var broser_sync_port 	= 3038;
var theme_location 		= 'public/wp-content/themes/'+wp_theme_name;
 
var paths = {
  styles: {
    src: [
		theme_location+'src/scss/style-template.scss',
		theme_location+'/src/scss/style.scss'
	],
	dest: theme_location,
	file: 'style.css'
  },
  scripts: {
    src: [
		'./node_modules/jquery/dist/jquery.min.js',
		'./node_modules/popper.js/dist/umd/popper.js',	
		'./node_modules/bootstrap/dist/js/bootstrap.min.js',
		'./node_modules/owl.carousel/dist/owl.carousel.min.js',
		theme_location+'/src/js/script.js'
	],
	dest: theme_location+'dist/js/',
	file: 'script.min.js'
  }
};
 
/* Not all tasks need to use streams, a gulpfile is just another node program
 * and you can use all packages available on npm, but it must return either a
 * Promise, a Stream or take a callback and call it
 */
function clean() {
  // You can use multiple globbing patterns as you would with `gulp.src`,
  // for example if you are using del 2.0 or above, return its promise
  return del([ 'assets' ]);
}


function reload(done) {
	browserSync.reload();
	done();
}
  
function serve(done) {
	browserSync.init({
		host: broser_sync_proxy,
		proxy: broser_sync_proxy,
		port: broser_sync_port,
		open: false
	});
	done();
}

/*
 * Define our tasks using plain functions
 */
function styles() {
  return gulp.src(paths.styles.src)
  	.pipe(plumber())
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		// .pipe(postcss([autoprefixer(), cssnano()]))
		.pipe(cleanCSS())
		.pipe(concat(paths.styles.file))
		.pipe(gulp.dest(paths.styles.dest))
		.pipe(browserSync.stream());
}

function scripts() {
	return gulp.src(paths.scripts.src, { sourcemaps: true })
		.pipe(plumber())
		.pipe(babel())
		.pipe(uglify())
		.pipe(concat(paths.scripts.file))
		.pipe(gulp.dest(paths.scripts.dest))
		.pipe(browserSync.stream());
  }
 
function watch() {
	gulp.watch(theme_location+'/src/js/**/*.js', scripts);
	gulp.watch(theme_location+'/src/scss/**/*.scss', styles);
	gulp.watch(theme_location+'/src/css/**/*.css', styles);
	// gulp.watch('scripts', reload);
}
 
/*
 * You can use CommonJS `exports` module notation to declare tasks
 */
exports.clean = clean;
exports.styles = styles;
// exports.browserSync = browserSync;
exports.scripts = scripts;
exports.watch = watch;
 
/*
 * Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
 */
var build = gulp.series(clean, gulp.parallel(styles, scripts, serve, watch));
 
/*
 * You can still use `gulp.task` to expose tasks
 */
gulp.task('build', build);
 
/*
 * Define default task that can be called by just running `gulp` from cli
 */
gulp.task('default', build);