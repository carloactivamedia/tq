(function($) {
	'use strict';

	var header = $("#header-wrapper .sticky-header").height();
	
	$('[data-toggle="tooltip"]').tooltip();

	// fix_grid_height();

	// setTimeout(function(){
	// 	fix_grid_height();
	// }, 2000);

	// $(".slider-item")
	// .mouseover(function(){
	// 	$('.loading-bar-container .loading-bar').stop();
	// })
	// .mouseout(function(){
	// 	// $('.loading-bar-container .loading-bar').stop();
	// });
	
	var homepage_carousel = $("#homepage-slider");
	homepage_carousel.on('initialized.owl.carousel', function(event){

		$('.owl-item.center .slider-item .loading-bar-container .loading-bar').animate({width: '100%'}, 5000);

		if($(".slider-navigation.prev").length > 0) {
			var next_elem = homepage_carousel.find('.owl-item.active.center').prev('.owl-item');
			$(".slider-navigation.prev").find('.post-category').html( $.trim(next_elem.find('.post-category').html()) );
			$(".slider-navigation.prev").find('.post-title').html( $.trim(next_elem.find('.post-title').html()) );
		}

		if($(".slider-navigation.next").length > 0) {
			var next_elem = homepage_carousel.find('.owl-item.active.center').next('.owl-item');
			$(".slider-navigation.next").find('.post-category').html( $.trim(next_elem.find('.post-category').html()) );
			$(".slider-navigation.next").find('.post-title').html( $.trim(next_elem.find('.post-title').html()) );
		}
	});
	
	homepage_carousel.on('translated.owl.carousel', function(event){ 
		$('.loading-bar-container .loading-bar').stop().css('width', '0');
		$('.owl-item.center .slider-item .loading-bar-container .loading-bar').animate({width: '100%'}, 5000);

		if($(".slider-navigation.prev").length > 0) {
			var next_elem = homepage_carousel.find('.owl-item.active.center').prev('.owl-item');
			$(".slider-navigation.prev").find('.post-category').html( $.trim(next_elem.find('.post-category').html()) );
			$(".slider-navigation.prev").find('.post-title').html( $.trim(next_elem.find('.post-title').html()) );
		}

		if($(".slider-navigation.next").length > 0) {
			var next_elem = homepage_carousel.find('.owl-item.active.center').next('.owl-item');
			$(".slider-navigation.next").find('.post-category').html( $.trim(next_elem.find('.post-category').html()) );
			$(".slider-navigation.next").find('.post-title').html( $.trim(next_elem.find('.post-title').html()) );
		}
	});

	homepage_carousel.owlCarousel({
        loop:true,
        margin:10,
		center:true,
		autoplay:true,
		autoplayTimeout:5000,
		// autoplayHoverPause:true,
        responsive:{
            0:{
				items:1
			},
			992: {
				items:3,
				autoWidth:true
			}
        }
	});

	$('.slider-navigation.prev').click(function() {
		homepage_carousel.trigger('prev.owl.carousel');
	});

	$('.slider-navigation.next').click(function() {
		homepage_carousel.trigger('next.owl.carousel');
	});

	var specialist_opinion_carousel = $("#specialist-opinion-slider");
	specialist_opinion_carousel.owlCarousel({
		items: 1
	});

	$('.sidebar-navigation.specialist.prev').click(function() {
		specialist_opinion_carousel.trigger('prev.owl.carousel');
	});

	$('.sidebar-navigation.specialist.next').click(function() {
		specialist_opinion_carousel.trigger('next.owl.carousel');
	});
	
	var featured_carousel = $("#featured-slider");
	featured_carousel.owlCarousel({
		items: 1,
		margin: 10,
        responsive:{
            0:{
				items:1
			},
			768: {
				items:2
			},
			992: {
				items:1
			}
        }
	});

	$('.sidebar-navigation.featured.prev').click(function() {
		featured_carousel.trigger('prev.owl.carousel');
	});

	$('.sidebar-navigation.featured.next').click(function() {
		featured_carousel.trigger('next.owl.carousel');
	});

	if (jQuery(this).scrollTop() > 1) {
		$("#header-wrapper").addClass('fixed');
	}

	// if($(".form-container").length > 0) {
	// 	$.ajax({
	// 		type: 'post',
	// 		url: wpajax.ajax_url,
	// 		data: 'action=search_form&s='+get_url_parameter('s'),
	// 		dataType: 'json',
	// 		success: function(res){
	// 			$(".form-container").html(res.html);
	// 		}
	// 	});
	// }

	// $(".rss-container").html(res.html);
	// $(".form-container").html(res.html);

	jQuery(window).scroll(function () {
		var w_width = $(window).width();
		if(w_width <= 425) {
			header = $("#header-wrapper .sticky-header").height();
		} else {
			header = $("#header-wrapper").height();
		}

		if (jQuery(this).scrollTop() >= (header + 8)) {
			// $("#content-wrapper").css('padding-top', (header + 8));
			$("#header-wrapper").addClass('fixed');
		} else {
			// $("#content-wrapper").css('padding-top', 0);
			$("#header-wrapper").removeClass('fixed');
		}
	});

	// jQuery(window).reisze(function () {
	// 	header = $("#header-wrapper .sticky-header").height();
	// });

	$('#to-top').on('click', function () {
		jQuery('html, body').animate({scrollTop: '0px'}, 800);
		return false;
	});

	/**
	 * Fix grid height
	 */
	function fix_grid_height() {
		if(jQuery('.fix-height').length > 0) {
			// Resize the size
			jQuery('.fix-height').height('auto');

			// Only higher than Tablet Viewport
			if(jQuery(window).width() >= 768) {
				if(jQuery('.fix-height:not([data-fix-height-group])').length > 0) {
					var d = new Date();
					var n = d.getTime();
					jQuery('.fix-height:not([data-fix-height-group])').each(function() {
						jQuery(this).attr('data-fix-height-group', n);
					});
				}

				var untrimmed_groups = jQuery('.fix-height').map(function() {
					return jQuery(this).attr('data-fix-height-group');
				}).get();

				var trimmed_groups = untrimmed_groups.filter( only_unique_values );

				for (var i = trimmed_groups.length - 1; i >= 0; i--) {

					var selector = jQuery('.fix-height[data-fix-height-group="'+trimmed_groups[i]+'"]');

					var elementHeights = selector.map(function() {
						return jQuery(this).height();
					}).get();

					// Math.max takes a variable number of arguments
					// `apply` is equivalent to passing each height as an argument
					var maxHeight = Math.max.apply(null, elementHeights);

					// Set each height to the max height
					selector.height(maxHeight);
				}
			}
		}
	}

	function only_unique_values(value, index, self) { 
		return self.indexOf(value) === index;
	}

	function get_url_parameter(name) {
		return decodeURI(
			(RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
	}
	
}(jQuery));