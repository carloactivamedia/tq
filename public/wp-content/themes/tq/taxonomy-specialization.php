<?php get_header(); ?>

<div id="content-wrapper">
	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="page-section">
			
		<div class="header-banner-wrapper">
			<div class="container">
				<div class="header-banner">
					<h3 class="category-title">
						Specialization
					</h3>
					<h1 class="post-title"><?php echo single_cat_title( '', false ) ?></h1>
				</div>
			</div>
		</div>
		
		<div class="doctor-panel-section-wrapper">
			<div class="container">
				<div class="section related-posts-section">
					<?php $category = get_queried_object(); ?>
					<?php
						$args = array( 
								'post_type'         => 'doctor',
								'post_status'	    => 'publish',
								'orderby' 			=> 'name',
								'order' 			=> 'ASC',
								'tax_query' 		=> array(
																array(
																	'taxonomy' => $category->taxonomy,
																	'field' => 'id',
																	'terms' => $category->term_id
																)
															)
						);
				
						$get_doctors = new wp_query( $args );
					?>
					<?php if($get_doctors->have_posts()) { ?>
						<div class="specialist-opinions-section doctor-panel-section">
							<div class="specialist-opinions">
								<div class="row">
									<?php while ( $get_doctors->have_posts() ) : $get_doctors->the_post(); ?>
										<?php 
											$post_thumbnail_id  = get_post_thumbnail_id( $get_doctors->post->ID );
											$thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
											$thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
										?>
										<?php $doctor = get_doctor_detail($get_doctors->post->ID); ?>

										<div class="col-lg-6 col-md-6 opinion-outer-wrapper">
											<div class="opinion-wrapper">
												<div class="opinion">
													
													<a class="opinion-thumbnail" href="<?php echo $doctor['doctor_link'] ?>" style="background-image: url('<?php echo @$doctor['avatar'] ?>')"></a>
													<div class="opinion-detail">
														<div class="opinion-title">
															<a href="<?php echo get_permalink($get_doctors->post->ID) ?>"><?php echo $get_doctors->post->post_title ?></a>
														</div>
														<div class="opinion-author">
															<div class="author-detail">
																<div class="author-position">
																	<?php echo wpautop($doctor['address']) ?>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
										
									<?php endwhile; ?>
								</div>
							</div>
						</div>

						<?php cs_pagination($wp_query->max_num_pages) ?>
					<?php } else { ?>
						<h3 class="text-center">No posts to display</h3>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

</div>

<?php get_footer(); ?>