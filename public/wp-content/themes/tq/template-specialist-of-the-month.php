<?php /** Template Name: Specialist Of The Month */ ?>
<?php get_header(); ?>

<div id="content-wrapper">
	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="page-section">
			
		<div class="header-banner-wrapper">
			<div class="container">
				<div class="header-banner">
					<h1 class="post-title"><?php echo get_the_title() ?></h1>
					<div class="post-subtitle"><?php echo get_the_content() ?></div>
				</div>
			</div>
		</div>
		
		<div class="doctor-panel-section-wrapper">
			<div class="container">
				<?php $category = get_queried_object(); ?>
				<?php
					$args = array( 
							'post_type'         => 'specialist_ofd_month',
							'post_status'	    => 'publish',
							'orderby' 			=> 'date',
							'order' 			=> 'DESC'
					);
			
					$get_sotm = new wp_query( $args );
				?>
				<div class="section list-section">
					<?php if($get_sotm->have_posts()) { ?>
						<div class="list-post">
	
							<div class="post-detail-wrapper">
								<?php while ( $get_sotm->have_posts() ) : $get_sotm->the_post(); ?>
									<?php 
										$post_thumbnail_id  = get_post_thumbnail_id( $get_sotm->post->ID );
										$thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
										$thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
									?>
									<div class="list-item">
										<div class="row">
											<div class="col-lg-6 order-lg-2">
												<a href="<?php echo get_permalink($get_sotm->post->ID) ?>" class="post-thumbnail-block">
													<div class="post-thumbnail" style="background-image: url('<?php echo $thumbnail ?>')"></div>
												</a>
											</div>
											<div class="col-lg-6 order-lg-1">
												<div class="post-detail">
													<div class="post-title">
														<a href="<?php echo get_permalink($get_sotm->post->ID) ?>"><?php echo get_the_title(); ?></a>
													</div>
													<div class="post-data-attribute">
														<ul class="list-inline">
															<?php $doctor = get_post_doctor_detail( $get_sotm->post->ID ); ?>
															<?php if($doctor) { ?>
																<li class="list-inline-item">
																	<div class="post-category">
																		By: 
																			<strong><?php echo $doctor['name']; ?></strong>
																	</div>
																</li>
															<?php } ?>
															
															<?php $terms = wp_get_post_terms( $get_sotm->post->ID, array( 'category' ) ); ?>
															<?php if($terms) { ?>
																<!-- <li class="list-inline-item">
																	<div class="post-category">
																		<?php foreach ( $terms as $term ) : ?>
																			<a href="<?php echo get_term_link($term->term_id) ?>"><?php echo $term->name; ?></a>
																		<?php endforeach; ?>
																	</div>
																</li> -->
															<?php } ?>
															<li class="list-inline-item">
																<div class="post-date">
																	<i class="far fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', $get_sotm->post->ID); ?>
																</div>
															</li>
															<li class="list-inline-item">
																<div class="reading-time">
																	<i class="far fa-clock"></i> <?php echo reading_time($get_sotm->post->post_content); ?>
																</div>
															</li>
														</ul>
													</div>
													<div class="post-excerpt">
														<?php echo short_description($get_sotm->post->ID, $get_sotm->post->post_content); ?>
													</div>
	
													<div class="read-more">
														<a href="<?php echo get_permalink($get_sotm->post->ID) ?>"><span class="go-to-arrow">Read More <i class="far fa-arrow-alt-circle-right"></i></span></a>
													</div>
												</div>
											</div>
											
										</div>
									</div>
								<?php endwhile; ?>
								<?php wp_reset_postdata() ?>
							</div>
	
						</div>
						<?php cs_pagination($wp_query->max_num_pages) ?>
					<?php } else { ?>
						<h3 class="text-center">No posts to display</h3>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

</div>

<?php get_footer(); ?>