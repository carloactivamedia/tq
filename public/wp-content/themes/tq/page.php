<?php get_header(); ?>

<div id="content-wrapper">

	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="page-section">

		<div class="header-banner-wrapper">
			<div class="container">
				<div class="header-banner">
					<h1 class="post-title"><?php echo get_the_title( '', false ) ?></h1>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1 col-sm-12">
					<div class="content">
						<?php
							while ( have_posts() ) : the_post();
								the_content();
							endwhile;
						?>
					</div>
				</div>
			</div>
		</div>

	</div> <!-- page-section -->

</div> <!-- content-wrapper -->

<?php get_footer(); ?>