<?php get_header(); ?>

<div id="content-wrapper">

	<?php echo get_template_part('template-parts/homepage-banner'); ?>

	<?php echo get_template_part('template-parts/specialist-of-the-month'); ?>

	<?php echo get_template_part('template-parts/featured-specialist-opinions'); ?>

	<?php echo get_template_part('template-parts/banners/canary-diamond') ?>

	<?php echo get_template_part('template-parts/travel-section') ?>

	<?php echo get_template_part('template-parts/banners/bmw-alpina') ?>

	<?php echo get_template_part('template-parts/food-section') ?>

	<?php echo get_template_part('template-parts/banners/liuli') ?>

	<?php echo get_template_part('template-parts/lifestyle-section') ?>

	<?php echo get_template_part('template-parts/medical-concierge-banner') ?>
	
	<div class="section subscribe-section" style="background: #2b2b2b url('<?php echo get_bloginfo('template_url') ?>/dist/images/diamond-pattern.jpg') repeat center top;">
		<div class="container">
			<div class="section-title">
				Subscribe to the TQ Newsletter
			</div>
			<div class="section-subtitle">
				For the latest healthcare and lifestyle offerings, subscribe to our newsletter
			</div>
			<div class="section-content">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<form action="" class="form-horizontal">
							<div class="form-group">
								<input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter Email Address">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-site subs">Subscribe</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

<?php get_footer() ?>