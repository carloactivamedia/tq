<?php 
// error_reporting(E_ALL);
ini_set('display_errors', 1);

// @ini_set( 'upload_max_size' , '64M' );
// @ini_set( 'post_max_size', '64M');
// @ini_set( 'max_execution_time', '300' );


/**
 * HELPER
 */

require_once('includes/helper/wp_bootstrap_navwalker.php');
require_once('includes/helper/function-helper.php');
require_once('includes/shortcodes/shortcodes.php');

register_nav_menus(array(
	'primary' 		=> __( 'Primary Menu' ),
	'footer_links'  => __( 'Footer Links' )
));



// Custom Scripting to Move JavaScript from the Head to the Footer
function remove_head_scripts() { 
	remove_action('wp_head', 'wp_print_scripts'); 
	remove_action('wp_head', 'wp_print_head_scripts', 9); 
	remove_action('wp_head', 'wp_enqueue_scripts', 1);
	remove_action('wp_head', 'wp_generator');
  
	add_action('wp_footer', 'wp_print_scripts', 5);
	add_action('wp_footer', 'wp_enqueue_scripts', 5);
	add_action('wp_footer', 'wp_print_head_scripts', 5); 
 } 
 add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );
 
/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


function current_to_active($classes) {
	$classes = str_replace('current-menu-item', 'current-menu-item active', $classes);
	return $classes;
}
add_filter('wp_nav_menu', 'current_to_active');


function frontend_scripts_and_styles(){
	wp_register_script('script', get_template_directory_uri().'/dist/js/script.min.js', array(), '2.1.20', true);
	wp_enqueue_script('script');
	
	wp_register_style('styles', get_stylesheet_uri(), array(), '2.2.4');
	wp_enqueue_style('styles');

	/* removed unnecessary scripts */
	if(!is_admin()) {
		// wp_deregister_script('jquery');
		wp_deregister_script('wp-embed');

		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/*  */
	wp_localize_script('script', 'wpajax', array(
		'ajax_url' 	 => admin_url('admin-ajax.php'), 
		'ajax_nonce' => wp_create_nonce('wp_nonce')
	));
}
add_action('wp_enqueue_scripts', 'frontend_scripts_and_styles' );


function wpb_comment_reply_text( $link ) {
$link = str_replace( 'Reply', '<i class="fa fa-comment"></i> Reply', $link );
return $link;
}
add_filter( 'comment_reply_link', 'wpb_comment_reply_text' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function mytheme_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
		<div id="comment-<?php comment_ID(); ?>" class="comment-wrapper">
			<div class="comment-author vcard">
				<?php echo get_avatar($comment, $args['avatar_size'] ); ?>
			</div>

			<div class="comment-content">
				<?php if ($comment->comment_approved == '0') : ?>
					<em><?php _e('Your comment is awaiting moderation.') ?></em>
					<br />
				<?php endif; ?>

				<div class="comment-meta commentmetadata">
					<?php printf(__('<div class="comment-author-name">%s</div>'), get_comment_author_link()) ?>
					
					<div class="comment-post-date">
						<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
							<?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?>
						</a>
					</div>
					<?php edit_comment_link(__('(Edit)'),'  ','') ?>
				</div>

				<?php comment_text() ?>

				<div class="reply">
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				</div>
			</div>

		</div>
	</li>
<?php
}

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
	global $post;
	// return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read More...</a>';
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/*****************************************************************
//																//
// 						AJAX REQUEST       						//
//																//
*****************************************************************/
require_once('includes/frontend/request/form.php');
require_once('includes/frontend/request/cart.php');








/************************************************************************************************************************************************************************************
* 																										 	   																		*
*  																			 DASHBOARD CUSTOM FUNCTIONS 	   																		*
* 																										 	   																		*
*************************************************************************************************************************************************************************************/

/*****************************************************
//													//
//  				LOADS SUPPORT          			//
//													//
*****************************************************/

add_theme_support( 'post-thumbnails' );


/**
 * Editor Configuration
 */
require_once('includes/core/editor/tinymce-config.php');

/**
 * Email Function
 */
// require_once('includes/core/email/email.php');

/**
 * INITIALIZE CPT
 */
require_once('includes/core/cpt/post-additional-fields.php');
require_once('includes/core/cpt/cmb-doctor-panel-fields.php');
require_once('includes/core/cpt/cmb-specialization-fields.php');
require_once('includes/core/cpt/category-fields.php');
require_once('includes/core/cpt/ads_manager-fields.php');
require_once('includes/core/cpt/specialist-of-the-month-fields.php');



/**
 * DASHBOARD MENU
 */
// require_once('includes/core/dashboard-menu/request-route.php');
// require_once('includes/core/dashboard-menu/update-recipients.php');

function getPrevNext(){
	$pagelist = get_pages('sort_column=menu_order&sort_order=asc');
	$pages = array();
	foreach ($pagelist as $page) {
	   $pages[] += $page->ID;
	}

	$current = array_search(get_the_ID(), $pages);
	$prevID = $pages[$current-1];
	$nextID = $pages[$current+1];
	
	echo '<div class="navigation">';
	
	if (!empty($prevID)) {
		echo '<div class="alignleft">';
		echo '<a href="';
		echo get_permalink($prevID);
		echo '"';
		echo 'title="';
		echo get_the_title($prevID); 
		echo'">Previous</a>';
		echo "</div>";
	}
	if (!empty($nextID)) {
		echo '<div class="alignright">';
		echo '<a href="';
		echo get_permalink($nextID);
		echo '"';
		echo 'title="';
		echo get_the_title($nextID); 
		echo'">Next</a>';
		echo "</div>";		
	}
}	


// add_filter( 'taxonomy_parent_dropdown_args', 'alter_parent_taxonomy', 10, 2 );
// function alter_parent_taxonomy( $args, $taxonomy ) {

//     if ( 'doctors' != $taxonomy ) return $args; // no change

//     $args['taxonomy'] = 'specialization';

//     return $args;
// }


// function custom_taxonomies_rewrite(){
//     add_rewrite_rule('^book/tag/([^/]*)/?','	book_tag=$matches[1]','top');
// }
// add_action('init','custom_taxonomies_rewrite');

add_filter('post_type_link', 'doctors_panel_term_permalink', 10, 4);
function doctors_panel_term_permalink($post_link, $post, $leavename, $sample)
{
    if ( false !== strpos( $post_link, '%specialization%' ) ) {
        $doctors_panel_letter = get_the_terms( $post->ID, 'specialization' );
        $post_link = str_replace( '%specialization%', array_pop( $doctors_panel_letter )->slug, $post_link );
    }
	return $post_link;
}

add_rewrite_rule('^doctors_panel/([^/]*)?$','index.php?doctors-tax=$matches[1]','top');
add_rewrite_rule('^doctors_panel/([^/]*)/([^/]*)?$','index.php?doctors-term=$matches[2]','top');
