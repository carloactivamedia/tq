<?php get_header(); ?>

<div id="content-wrapper">
	<div class="page-section section">
		<div class="container">

			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', 'page' );
			endwhile;
			?>

			<h2>Page Under Construction</h2>

		</div>
	</div>
</div>

<?php get_footer(); ?>