<?php get_header(); ?>

<div id="content-wrapper">
	
	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="page-section">

		<div class="header-banner-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-lg-10 offset-lg-1 col-sm-12">
						<div class="header-banner">
							<?php 
								$post_categories = get_post_primary_category(get_the_ID(), 'category'); 
								$primary_category = $post_categories['primary_category'];
								if(!empty($primary_category)) {
							?>
							<h3 class="category-title">
								<?php echo $primary_category->name; ?>
							</h3>
							<?php } ?>
							<h1 class="post-title"><?php echo get_the_title() ?></h1>
							<div class="post-attributes">
								<ul>
									<li>
										<?php $doctor = get_post_doctor_detail( get_the_ID() ); ?>
										<?php if($doctor) { ?>
											<li class="list-inline-item">
												<div class="post-category">
													By: 
														<strong><?php echo $doctor['name']; ?></strong>
												</div>
											</li>
										<?php } ?>
									</li>
									<li><i class="far fa-calendar-alt"></i> <?php echo get_the_date() ?></li>
									<li><span data-toggle="tooltip" data-placement="top" data-html="true"><i class="far fa-clock"></i> <?php echo reading_time(get_the_content()); ?></span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php $page_banner = get_post_meta(get_the_ID(), 'cmb2_page_banner', true); ?>
		<?php if($page_banner) { ?>
		<div class="page-banner-wrapper">
			<div class="container">
				<img src="<?php echo $page_banner ?>" class="img-fluid w-100">
			</div>
		</div>
		<?php } ?>
		
		<div class="page-content-wrapper">
			<div class="container">
				<div class="row">
					
					<div class="col-lg-9">
						<div class="page-content">
							<?php
								get_template_part('template-parts/share-buttons');

								while ( have_posts() ) : the_post();
									echo do_shortcode(the_content());
								endwhile;
							?>
						</div>
					</div>

					<div class="col-lg-3">

						<div class="sidebar-wrapper">
							<?php if(cat_is_ancestor_of(3, $primary_category->term_id)) { ?>
								<?php echo get_template_part('template-parts/sidebar-specialist-opinion-static') ?>
							<?php } else { ?>
								<?php echo get_template_part('template-parts/sidebar-static') ?>
							<?php } ?>

						</div>

					</div>

				</div>
			</div>
		</div>

	</div>

	<?php echo get_template_part('template-parts/other-articles') ?>

</div>

<?php get_footer(); ?>