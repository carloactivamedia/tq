<?php get_header(); ?>

<div id="content-wrapper">
	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="page-section">
			
		<div class="header-banner-wrapper">
			<div class="container">
				<div class="header-banner">
					<h3 class="category-title">
						Doctors Panel
					</h3>
					<h1 class="post-title"><?php echo get_the_title() ?></h1>
				</div>
			</div>
		</div>
		
		<div class="doctor-panel-section-wrapper">
			<div class="container">
				<div class="section related-posts-section">
                    
                    <div class="specialist-opinions-section doctor-panel-section">
                        <div class="specialist-opinions">
                            <div class="row">

                                <?php $doctor = get_doctor_detail(get_the_ID()); ?>

                                <div class="col-lg-6 col-md-6 opinion-outer-wrapper">
                                    <div class="opinion-wrapper">
                                        <div class="opinion">

                                            <div class="opinion-thumbnail" style="background-image: url('<?php echo $doctor['avatar'] ?>')"></div>
                                            <div class="opinion-detail">
                                                <div class="opinion-title">
                                                    <a href="<?php echo $doctor['doctor_link'] ?>"><?php echo $doctor['name'] ?></a>
                                                </div>
                                                <div class="opinion-author">
                                                    <div class="author-detail">
                                                        <div class="author-position">
                                                            <?php echo wpautop($doctor['address']) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="header-banner-wrapper">
                        <div class="container">
                            <div class="header-banner">
                                <h1 class="post-title">Articles</h1>
                            </div>
                        </div>
                    </div>

                    <?php
                        $args = array( 
                                'post_type'         => array('post', 'specialist_ofd_month'),
                                'post_status'	    => 'publish',
                                'posts_per_page'    => -1,
                                'orderby' 			=> 'date',
                                'order' 			=> 'DESC',
                                'meta_query'        => array(
                                                            array(
                                                                'key'     => 'cmb2_post_multicheckbox',
                                                                'value'   => get_the_ID(),
                                                                'compare' => '=',
                                                            ),
                                                        )
                        );

                        $get_articles = new wp_query( $args );
                    ?>
                    <?php if($get_articles->have_posts()) { ?>
                        <div class="section related-posts-section pt-1">
                            <div class="related-posts">
                                <div class="row">
                                    <?php while ( $get_articles->have_posts() ) { $get_articles->the_post(); ?>
                                    
                                        <div class="col-xl-3 col-lg-4 col-md-6 other-post-wrapper">
                                            <?php
                                                $post_thumbnail_id  = get_post_thumbnail_id( $get_articles->post->ID );
                                                $thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
                                                $thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
                                            ?>
                                            <a href="<?php echo get_permalink($get_articles->post->ID) ?>">
                                                <div class="other-post" style="background-image:url('<?php echo @$thumbnail ?>')">
                                                    <div class="reading-time">
                                                        <i class="far fa-clock"></i> <?php echo reading_time($get_articles->post->post_content); ?>
                                                    </div>
                                                    <div class="other-post-detail">
                                                        <div class="other-post-title">
                                                            <?php echo $get_articles->post->post_title ?>
                                                        </div>
                                                        <div class="other-post-category">
                                                            <?php
                                                                if($get_articles->post->post_type == 'post') {
                                                                    $get_category = get_the_category($get_articles->post->ID);
                                                                    echo $get_category[0]->name;
                                                                } else {
                                                                    $get_category = get_post_type_object($get_articles->post->post_type);
                                                                    echo $get_category->label;
                                                                }
                                                            ?>
                                                        </div>
                                                        <div class="other-post-date">
                                                            <i class="far fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', $get_articles->post->ID); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php wp_reset_postdata() ?>

				</div>
			</div>
		</div>

        


	</div>

</div>

<?php get_footer(); ?>