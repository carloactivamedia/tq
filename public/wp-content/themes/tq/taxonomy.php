<?php get_header(); ?>

<div id="content-wrapper">
sss
	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="page-section">
			
		<div class="header-banner-wrapper">
			<div class="container">
				<div class="header-banner">
					<h1 class="post-title"><?php echo single_cat_title( '', false ) ?></h1>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-xl-12">

					<div class="section grid-section">
						<?php if(have_posts()) { ?>
							<div class="grid-post">
								<div class="row">

									<?php while ( have_posts() ) : the_post(); ?>
										<?php 
											$post_thumbnail_id  = get_post_thumbnail_id( get_the_ID() );
											$thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
											$thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
										?>

										<div class="col-lg-4 col-md-6">
											<div class="post-detail-wrapper">
												<a href="<?php echo get_permalink(get_the_ID()) ?>" class="post-thumbnail-block">
													<div class="post-thumbnail" style="background-image: url('<?php echo $thumbnail ?>')"></div>
												</a>
												<div class="post-detail">
													<div class="post-title">
														<a href="<?php echo get_permalink(get_the_ID()) ?>"><?php echo get_the_title(); ?></a>
													</div>
													<div class="post-data-attribute">
														<ul class="list-inline">
															<?php $doctors = wp_get_post_terms( get_the_ID(), array( 'doctors' ) ); ?>
															<?php if($doctors) { ?>
																<li class="list-inline-item">
																	<div class="post-category">
																		By: 
																		<?php foreach ( $doctors as $doctor ) : ?>
																			<strong><?php echo $doctor->name; ?></strong>
																		<?php endforeach; ?>
																	</div>
																</li>
															<?php } ?>
															
															<?php $terms = wp_get_post_terms( get_the_ID(), array( 'category' ) ); ?>
															<?php if($terms) { ?>
																<!-- <li class="list-inline-item">
																	<div class="post-category">
																		<?php foreach ( $terms as $term ) : ?>
																			<a href="<?php echo get_term_link($term->term_id) ?>"><?php echo $term->name; ?></a>
																		<?php endforeach; ?>
																	</div>
																</li> -->
															<?php } ?>

															<li class="list-inline-item">
																<div class="reading-time">
																	<i class="far fa-clock"></i> <?php echo reading_time(get_the_content()); ?>
																</div>
															</li>
														</ul>
													</div>
													<div class="post-excerpt">
														<?php the_excerpt(); ?>
													</div>

													<div class="read-more">
														<a href="<?php echo get_permalink(get_the_ID()) ?>"><span class="go-to-arrow">Read More <i class="far fa-arrow-alt-circle-right"></i></span></a>
													</div>
												</div>
											</div>
										</div>
										
									<?php endwhile; ?>

								</div>

								<?php cs_pagination($wp_query->max_num_pages) ?>
							</div>
						<?php } else { ?>
							<h3 class="text-center">No posts to display</h3>
						<?php } ?>
					</div>

				</div>

				<!-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
					
					<div class="sidebar-wrapper">
						<?php //get_template_part( 'template-parts/sidebar/sidebar', 'connected' ); ?>

						<?php //get_template_part( 'template-parts/sidebar/sidebar', 'recent-posts' ); ?>

						<?php //get_template_part( 'template-parts/sidebar/sidebar', 'archives' ); ?>

						<?php //get_template_part( 'template-parts/sidebar/sidebar', 'search' ); ?>
					</div>

				</div> -->
			</div>
		</div>
	</div>

</div>

<?php get_footer(); ?>