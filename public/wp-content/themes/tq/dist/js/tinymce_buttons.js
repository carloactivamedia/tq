// ed.addButton('mybutton', {
//     title: 'My button', 
//     class: 'MyCoolBtn', 
//     image: 'MyCoolBtn.png', 
//     onclick: function() { 
//       // Add your own code to execute something on click 
//       ed.focus();
//       ed.selection.setContent('<tag>' + ed.selection.getContent() + '</tag>');
//     }
// });

(function() {
    tinymce.PluginManager.add( 'columns', function( editor, url ) {
        // Add Button to Visual Editor Toolbar
        editor.addButton('customText1', {
            title: 'Responsive Text 1 (Bigger)',
            cmd: 'responsive_text_1',
            image: url + '/columns.jpg',
        });
 
        editor.addCommand('responsive_text_1', function() {
            var selected_text = editor.selection.getContent({
                'format': 'html'
            });
            if ( selected_text.length === 0 ) {
                alert( 'Please select some text.' );
                return;
            }
            var open_column = '<p class="custom-text text-1"><span>';
            var close_column = '</span></p>';
            var return_text = '';
            return_text = open_column + selected_text + close_column;
            editor.execCommand('mceReplaceContent', false, return_text);
            return;
        });

        editor.addButton('customText2', {
            title: 'Responsive Text 2 (Smaller)',
            cmd: 'responsive_text_2',
            image: url + '/columns.jpg',
        });
 
        editor.addCommand('responsive_text_2', function() {
            var selected_text = editor.selection.getContent({
                'format': 'html'
            });
            if ( selected_text.length === 0 ) {
                alert( 'Please select some text.' );
                return;
            }
            var open_column = '<p class="custom-text text-2"><span>';
            var close_column = '</span></p>';
            var return_text = '';
            return_text = open_column + selected_text + close_column;
            editor.execCommand('mceReplaceContent', false, return_text);
            return;
        });
 
    });
})();