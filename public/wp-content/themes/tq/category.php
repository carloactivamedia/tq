<?php get_header(); ?>

<div id="content-wrapper">

	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>
	
	<div class="page-section">
			
		<div class="header-banner-wrapper">
			<div class="container">
				<div class="header-banner">
					<h1 class="post-title"><?php echo single_cat_title( '', false ) ?></h1>
					
					<?php $category = get_queried_object(); ?>
					<?php $category_id = $category->term_id; ?>

					<?php if(cat_is_ancestor_of(3, $category_id)) { ?>
						
						<div class="post-subtitle"><?php echo category_description() ?></div>

					<?php } else { ?>
						
						<?php $subtitle = get_term_meta($category_id, 'cmb2_subtitle', true) ?>
						<?php if($subtitle) { ?>
								<div class="post-subtitle"><?php echo wpautop($subtitle) ?></div>
						<?php } ?>

					<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="container">

			<?php if(cat_is_ancestor_of(3, $category_id)) { ?>
				
				<?php echo get_template_part('template-parts/specialist-opinions-grid') ?>

			<?php } else { ?>
				
				<div class="section list-section">
					<?php if(have_posts()) { ?>
						<div class="list-post">
	
							<div class="post-detail-wrapper">
								<?php while ( have_posts() ) : the_post(); ?>
									<?php 
										$post_thumbnail_id  = get_post_thumbnail_id( get_the_ID() );
										$thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
										$thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
									?>
									<div class="list-item">
										<div class="row">
											<div class="col-lg-6 order-lg-2">
												<a href="<?php echo get_permalink(get_the_ID()) ?>" class="post-thumbnail-block">
													<div class="post-thumbnail" style="background-image: url('<?php echo $thumbnail ?>')"></div>
												</a>
											</div>
											<div class="col-lg-6 order-lg-1">
												<div class="post-detail">
													<div class="post-title">
														<a href="<?php echo get_permalink(get_the_ID()) ?>"><?php echo get_the_title(); ?></a>
													</div>
													<div class="post-data-attribute">
														<ul class="list-inline">
															<?php $doctor = get_post_doctor_detail( get_the_ID() ); ?>
															<?php if($doctor) { ?>
																<li class="list-inline-item">
																	<div class="post-category">
																		By: 
																			<strong><?php echo $doctor['name']; ?></strong>
																	</div>
																</li>
															<?php } ?>
															
															<?php $terms = wp_get_post_terms( get_the_ID(), array( 'category' ) ); ?>
															<?php if($terms) { ?>
																<!-- <li class="list-inline-item">
																	<div class="post-category">
																		<?php foreach ( $terms as $term ) : ?>
																			<a href="<?php echo get_term_link($term->term_id) ?>"><?php echo $term->name; ?></a>
																		<?php endforeach; ?>
																	</div>
																</li> -->
															<?php } ?>
															<li class="list-inline-item">
																<div class="post-date">
																	<i class="far fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', get_the_ID()); ?>
																</div>
															</li>
															<li class="list-inline-item">
																<div class="reading-time">
																	<i class="far fa-clock"></i> <?php echo reading_time(get_the_content()); ?>
																</div>
															</li>
														</ul>
													</div>
													<div class="post-excerpt">
														<?php echo short_description(get_the_ID(), get_the_content()); ?>
													</div>
	
													<div class="read-more">
														<a href="<?php echo get_permalink(get_the_ID()) ?>"><span class="go-to-arrow">Read More <i class="far fa-arrow-alt-circle-right"></i></span></a>
													</div>
												</div>
											</div>
											
										</div>
									</div>
								<?php endwhile; ?>
								<?php wp_reset_postdata() ?>
							</div>
	
						</div>
						<?php cs_pagination($wp_query->max_num_pages) ?>
					<?php } else { ?>
						<h3 class="text-center">No posts to display</h3>
					<?php } ?>
				</div>

			<?php } ?>

		</div>

	</div> <!-- page-section -->

	<?php echo get_template_part('template-parts/other-articles') ?>

</div>

<?php get_footer(); ?>