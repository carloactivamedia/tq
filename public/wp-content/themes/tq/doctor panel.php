<?php get_header(); ?>

<div id="content-wrapper">

	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>
	
	<div class="page-section">
			
		<div class="header-banner-wrapper">
			<div class="container">
				<div class="header-banner">
					<h1 class="post-title"><?php echo single_cat_title( '', false ) ?></h1>
					<?php $category = get_queried_object(); ?>
					<?php $category_id = $category->term_id; ?>
					<?php $subtitle = get_term_meta($category_id, 'cmb2_subtitle', true) ?>
					
					<?php if($subtitle) { ?>
						<div class="post-subtitle"><?php echo wpautop($subtitle) ?></div>
					<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="container">
				
			<?php 
				$car_types_querys = get_categories([
					'child_of' 		=> 3
				]);
			?>

			<?php if(!empty($car_types_querys)) { ?>
				<div class="section related-posts-section">
					<div class="related-posts">
						<div class="row">
							<?php foreach ($car_types_querys as $category) { ?>
								<div class="col-xl-3 col-lg-4 col-md-6 other-post-wrapper">
									<?php $thumbnail = get_term_meta($category->term_id, 'cmb2_thumbnail', true) ?>
									<a href="<?php echo get_term_link($category->term_id) ?>">
										<div class="other-post" style="background-image:url('<?php echo @$thumbnail ?>')">
											<div class="other-post-detail">
												<h2 class="other-post-title">
													<?php echo $category->name ?>
												</h2>
											</div>
										</div>
									</a>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			<?php } ?>
			
		</div>
	</div>

	<?php echo get_template_part('template-parts/other-articles') ?>

</div>

<?php get_footer(); ?>