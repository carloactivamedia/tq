<?php get_header(); ?>

<div id="content-wrapper">

	<div class="header-banner-wrapper">
		<div class="header-banner">
			<div class="container">
				<div class="content-detail">
					<div class="primary-text">
						<?php echo single_cat_title( '', false ) ?>
					</div>
					<?php if ( function_exists('yoast_breadcrumb') ) { ?>
						<div class="breadcrumbs">
							<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">

				<div class="blog-grid-wrapper section">
					<?php if(have_posts()) { ?>

						<div class="row">

							<?php while ( have_posts() ) : the_post(); ?>
								<?php 
									$post_thumbnail_id  = get_post_thumbnail_id( get_the_ID() );
									// $thumbnail 		= wp_get_attachment_url($post_thumbnail_id);
									$thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'medium');
									$thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
								?>

								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
									<div class="blog-grid-block">
										<div class="blog-grid-image">
											<a href="<?php echo get_permalink(get_the_ID()) ?>">
												<img src="<?php echo $thumbnail ?>" class="img-fluid grid-image-tag">
												<div class="grid-image-bg" style="background-image: url('<?php echo $thumbnail ?>')"></div>
											</a>
										</div>
										<div class="blog-grid-title">
											<a href="<?php echo get_permalink(get_the_ID()) ?>"><?php echo get_the_title(); ?></a>
										</div>
										<div class="blog-grid-info">
											<?php the_author_nickname() ?> - <?php echo get_the_date() ?>
										</div>
										<div class="blog-grid-excerpt">
											<?php the_excerpt(); ?>
										</div>
									</div>
								</div>
								
							<?php endwhile; ?>

						</div>

						<?php cs_pagination($wp_query->max_num_pages) ?>

					<?php } ?>
				</div>

			</div>

			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
				
				<div class="sidebar-wrapper">
					<?php get_template_part( 'template-parts/sidebar/sidebar', 'connected' ); ?>

					<?php get_template_part( 'template-parts/sidebar/sidebar', 'recent-posts' ); ?>

					<?php get_template_part( 'template-parts/sidebar/sidebar', 'archives' ); ?>

					<?php get_template_part( 'template-parts/sidebar/sidebar', 'search' ); ?>
				</div>

			</div>
		</div>
	</div>

</div>

<?php get_footer(); ?>