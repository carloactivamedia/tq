<!-- FOOTER -->

<?php if(!is_front_page()) { ?>
	
	<?php if(is_page() != 273){ ?>
		<?php echo get_template_part('template-parts/medical-concierge-banner') ?>
	<?php } ?>
	
	<div class="section subscribe-section" style="background: #2b2b2b url('<?php echo get_bloginfo('template_url') ?>/dist/images/diamond-pattern.jpg') repeat center top;">
		<div class="container">
			<div class="section-title">
				Subscribe to the TQ Newsletter
			</div>
			<div class="section-subtitle">
				For the latest healthcare and lifestyle offerings, subscribe to our newsletter
			</div>
			<div class="section-content">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<!-- feedback.activamedia.com.sg script begins here --><script type="text/javascript" defer src="//feedback.activamedia.com.sg/embed/4485273.js" data-role="form" data-default-width="650px"></script><!-- feedback.activamedia.com.sg script ends here -->
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<div id="footer-wrapper">
	<div class="container">

		<div class="botton-footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-12 order-lg-2">
						<?php 
							if ( has_nav_menu( 'footer_links' ) ) :
								$args = array(
									'menu' 				=> 'footer_links',
									'theme_location'  	=> 'footer_links',
									'container_class'	=> 'footer-links');
								wp_nav_menu($args);
							endif;
						?>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<!-- <a id="cart-link" href="<?php echo get_bloginfo('url').'/enquiry-cart/' ?>"><i class="fas fa-shopping-cart"></i></a> -->

<!-- <div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div> -->

<?php wp_footer(); ?>

</body>
</html>