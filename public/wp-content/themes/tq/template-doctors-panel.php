<?php /* Template Name: Doctors Panel */ ?>
<?php get_header(); ?>

<div id="content-wrapper">

	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>
	
	<div class="page-section">
			
		<div class="header-banner-wrapper">
			<div class="container">
				<div class="header-banner">
					<h3 class="category-title">
						Specialist Opinions
					</h3>
					<h1 class="post-title"><?php echo get_the_title() ?></h1>
				</div>
			</div>
		</div>

		<div class="doctor-panel-section-wrapper">
			<div class="container">
	
				<div class="section related-posts-section">
		
					<?php
						$get_specializations = get_terms([
							'taxonomy' 		=> 'specialization',
							'hide_empty' 	=> false,
							'orderby' 		=> 'name',
							'order' 		=> 'ASC'
						]);
						$doctor_arr = [];
					?>
	
					<?php if(!empty($get_specializations)) { ?>
						<?php foreach ($get_specializations as $key => $specialization) { ?>
						
							<div class="specialization-text">
								<a href="<?php echo get_term_link($specialization->term_id) ?>"><?php echo $specialization->name; ?></a>
							</div>
							
							<?php
								$args = array( 
										'post_type'         => 'doctor',
										'post_status'	    => 'publish',
										'posts_per_page'    => -1,
										'orderby' 			=> 'name',
										'order' 			=> 'ASC',
										'tax_query' 		=> array(
																		array(
																			'taxonomy' => $specialization->taxonomy,
																			'field' => 'id',
																			'terms' => $specialization->term_id
																		)
																	)
								);
						
								$get_doctors = new wp_query( $args );
							?>
							<?php if($get_doctors->have_posts()) { ?>
								<div class="specialist-opinions-section doctor-panel-section">
									<div class="specialist-opinions">
										<div class="row">
											<?php while ( $get_doctors->have_posts() ) { $get_doctors->the_post(); ?>
												<?php $doctor = get_doctor_detail($get_doctors->post->ID); ?>
												<div class="col-lg-6 col-md-6 opinion-outer-wrapper">
													<div class="opinion-wrapper">
														<div class="opinion">

															<a class="opinion-thumbnail" href="<?php echo $doctor['doctor_link'] ?>" style="background-image: url('<?php echo @$doctor['avatar'] ?>')"></a>
															<div class="opinion-detail">
																<div class="opinion-title">
																	<a href="<?php echo $doctor['doctor_link'] ?>"><?php echo $doctor['name'] ?></a>
																</div>
																<div class="opinion-author">
																	<div class="author-detail">
																		<div class="author-position">
																			<?php echo wpautop($doctor['address']) ?>
																		</div>
																	</div>
																</div>
															</div>

														</div>
													</div>
												</div>
											<?php } ?>
											<?php wp_reset_postdata() ?>
										</div>
									</div>
								</div>
							<?php } else { ?>
								No doctor to display
							<?php } ?>

									
						<?php } ?>
					<?php } else { ?>
						<h3 class="text-center">No posts to display</h3>
					<?php } ?>
						
				</div>
				
			</div>
		</div>
		
	</div>

</div>

<?php get_footer(); ?>