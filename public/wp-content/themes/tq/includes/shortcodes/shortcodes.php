<?php

/**
 * name: col
 * usage: [col]xxxx[/col]
 */
function col_shortcode($atts, $content = null) {
	extract(
		shortcode_atts(
			array(
				'class' => 'col-lg-6'
			),
			$atts
		)
	);
	return '<div class="'.$class.'">'.$content.'</div>';
}
add_shortcode('col', 'col_shortcode');

/**
 * name: colContainer
 * usage: [colContainer]xxxx[/colContainer]
 */
function colContainer_shortcode($atts, $content = null) {
	return '<div class="container">
				<div class="row">
					'.do_shortcode($content).'
				</div>
			</div>';
}
add_shortcode('colContainer', 'colContainer_shortcode');

function url_shortcode() {
	return get_bloginfo('url');
}
add_shortcode('url','url_shortcode');

/**
 * name: color-box
 * usage: [color-box]xxxx[/color-box]
 */
function color_box_shortcode($atts, $content = null) {
	extract(
		shortcode_atts(
			array(
				'color' => 'col-lg-6'
			),
			$atts
		)
	);
	return '</div></div></div></div><div class="color-box" style="background-color:'.$color.'">'.$content.'</div><div class="container"><div class="row"><div class="col-lg-12"><div class="page-content">';
}
add_shortcode('color_box', 'color_box_shortcode');

function shortcode_empty_paragraph_fix( $content ) {

    // define your shortcodes to filter, '' filters all shortcodes
    $shortcodes = array( 'blueBlock' );

    foreach ( $shortcodes as $shortcode ) {

        $array = array (
            '<p>[' . $shortcode => '[' .$shortcode,
            '<p>[/' . $shortcode => '[/' .$shortcode,
            $shortcode . ']</p>' => $shortcode . ']',
            $shortcode . ']<br />' => $shortcode . ']'
        );

        $content = strtr( $content, $array );
    }

    return $content;
}
add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );

/**
 * name: title
 * usage: [title]xxxx[/title]
 */
function title_shortcode($atts, $content = null) {
	extract(
			shortcode_atts(
				array(
					'tag' => 'div',
					'style' => '',
					'align' => ''
				),
				$atts
			)
	);
	
	if($style == 'strikethrough') {
		$result = '<div class="strikethrough '.$align.'"><'.$tag.' class="custom-text text-1 '.$align.'">'.$content.'</'.$tag.'></div>';
	} else {
		$result = '<'.$tag.' class="custom-text text-1">'.$content.'</'.$tag.'>';
	}

	return $result;
}
add_shortcode('title', 'title_shortcode');

/**
 * name: blueBlock
 * usage: [blueBlock]xxxx[/blueBlock]
 */
function blueBlock_shortcode($atts, $content = null) {
	return '<div class="blue-block">'.$content.'</div>';
}
add_shortcode('blueBlock', 'blueBlock_shortcode');


/**
 * name: ad-box
 * usage: [ad-box]xxxx[/ad-box]
 */
function ad_box_shortcode($atts, $content = null) {
	extract(
			shortcode_atts(
				array(
					'type' => 'image',
					'image' => '',
					'folder' => '',
					'link' => ''
				),
				$atts
			)
	);

	if($type == 'image') {
		return '</div></div></div></div><div class="section addddd-section"><div class="container"><div class="section-title">ADVERTISEMENT</div><a href="'.$link.'" target="_blank"><img src="'.$image.'" class="img-fluid"></a></div></div><div class="container"><div class="row"><div class="col-lg-12"><div class="page-content">';
	} else {
		return  '</div></div></div></div><div class="section addddd-section"><div class="container"><div class="section-title">ADVERTISEMENT</div><div class="embed-responsive embed-responsive-21by9 d-none d-md-block">
					<iframe src="'.get_bloginfo('template_url').'/template-parts/banners/html/canary-diamond/desktop/index.html" class="embed-responsive-item" scrolling="no" width="100%" height="269" style="overflow:hidden; width: 100%; display:block;" frameborder="0"></iframe>
				</div>
				<div class="d-block d-md-none">
					<iframe src="'.get_bloginfo('template_url').'/template-parts/banners/html/canary-diamond/mobile/index.html" scrolling="no" width="100%" height="269" style="overflow:hidden; width: 100%; display:block;" frameborder="0"></iframe>
				</div>
				</div></div><div class="container"><div class="row"><div class="col-lg-12"><div class="page-content">';
	}
}
add_shortcode('ad-box', 'ad_box_shortcode');


/**
 * name: Video Tag
 * usage: [video]xxxx[/video]
 */
function video_shortcode($atts, $content = null) {
	// return '<div class="blue-block">'.$content.'</div>';
	extract(
			shortcode_atts(
				array(
					'width' => '',
					'height' => '',
					'mp4' => '',
					'poster' => ''
				),
				$atts
			)
	);
	// return '<div class="embed-responsive embed-responsive-1by1">
	// 			<video class="embed-responsive-item" controls="controls" poster="'.$poster.'" src="'.$mp4.'" type="video/mp4"></video>
	// 		</div>';
	return '<video style="width: 100%; max-width: 700px; height: auto;" controls="controls" poster="'.$poster.'" src="'.$mp4.'" type="video/mp4" onclick="this.play();"></video>';
}
add_shortcode('video', 'video_shortcode');