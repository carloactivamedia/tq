<?php
/**
 * -------------
 * String to URL
 * -------------
 * convert to seo friendly url
 * @param ($string) string
 * @param ($separator) separator used. - by default 
 */
function url_string($string, $separator = '-'){
	$accents = array('Š' => 'S', 'š' => 's', 'Ð' => 'Dj','Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss','à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f');
	$string = strtr($string, $accents);
	$string = strtolower($string);
	$string = preg_replace('/[^a-zA-Z0-9\s]/', '', $string);
	$string = preg_replace('{ +}', ' ', $string);
	$string = trim($string);
	$string = str_replace(' ', $separator, $string);

	return strtolower($string);
}

/**
 * -------------
 * Get Client IP
 * -------------
 * Get client ip address
 */
function get_client_ip_server(){
	$ipaddress = '';
	if($_SERVER['HTTP_CLIENT_IP']){
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	} else if($_SERVER['HTTP_X_FORWARDED_FOR']){
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else if($_SERVER['HTTP_X_FORWARDED']){
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	} else if($_SERVER['HTTP_FORWARDED_FOR']){
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	} else if($_SERVER['HTTP_FORWARDED']){
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	} else if($_SERVER['REMOTE_ADDR']){
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	} else {
		$ipaddress = 'UNKNOWN';
	}
	return $ipaddress;
}

/**
 * ------------
 * Styled Array
 * ------------
 * Styled array. debugging purposes
 * @param ($array) array item(s)
 */
function dd($array) {
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

function current_url()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	return $protocol.$domainName;
}

function appendQueryString($query)
{
	// $query_parts explode($query)
	if(!is_array($query)) {
		return false;
	}

	$url_parts = parse_url(current_url());
	if(@$url_parts['query']) {
		parse_str($url_parts['query'], $params);
	}

	foreach ($query as $key => $string) {
		$params[$key] = $string;
	}

	$url_parts['query'] = http_build_query($params);

	return $url_parts['scheme'] . '://' . $url_parts['host'] . $url_parts['path'] . '?' . $url_parts['query'];
}

/**
 * ---------------
 * Get URL Segment
 * ---------------
 * Generate excerpt
 * @param ($segment) number of segment
 */
function url_segment($segment = false){
	if($segment == false) return false;
	$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri_segments = explode('/', $uri_path);
	return $uri_segments[$segment];
}

/**
 * -------
 * Excerpt
 * -------
 * Generate excerpt
 * @param ($string) string
 * @param ($length) default by 10, set length
 * @param ($trailing) trailing output
 */
function excerpt($string, $length = 10, $trailing='...'){
	$length-=mb_strlen($trailing);
	if (mb_strlen($string) > $length) return mb_substr($string, 0, $length).$trailing;
	return $string;
}

/**
 * ----------
 * Datatables
 * ----------
 * Render Datatable
 * @param ($table) table name
 * @param ($columns) table columns
 * @param ($col_search) searchable columns
 */
function render_dt_data($table, $columns, $col_search){
	global $wpdb;
	$request  = $_POST;

	// Get Records without any search
	$result         = $wpdb->get_results("SELECT * FROM $table WHERE session_key != ''", OBJECT);
	$total_data     = count($result);
	$total_filtered = $total_data;

	// For Search Result(s)
	$sql = "SELECT * FROM $table WHERE session_key != ''";
	if(!empty($request['search']['value'])){
		$i = 0;
		foreach ($col_search as $key => $item){
			if($request['search']['value']){
				if($i===0){
					$sql.=' AND ( `'.$item.'` LIKE "%'.$request['search']['value'].'%"';
				} else {
					$sql.=' OR `'.$item.'` LIKE "%'.$request['search']['value'].'%"';
				}
				if(count($col_search) - 1 == $i)
				$sql .= ')';
			}
			$i++;
		}
	}
	$result = $wpdb->get_results($sql);
	$total_filtered = count($result);

	// For Pagination - Order by columns[0]
	$sql .= ' ORDER BY '.$columns[$request['order'][0]['column']].' '.$request['order'][0]['dir'];
	if($request['length'] != -1)
	$sql .= ' LIMIT '.intval($request['start']).','.intval($request['length']);
	$result = $wpdb->get_results($sql);

	// Changing Format(Optional)
	foreach ($columns as $key => $value) {
		foreach ($result as $key2 => $col) {
			$data[$key2][$value] .= ($value == 'date_created' ? date('M d, Y', strtotime($col->$value)) : $col->$value);
		}
	}

	$json_data = array(
		'draw'            => intval($request['draw']),
		'recordsTotal'    => intval($total_data),
		'recordsFiltered' => intval($total_filtered ),
		'data'            => !empty($data) ? $data : 0
	);

	return $json_data;
}

/**
 * --------------
 * CPT Pagination
 * --------------
 * Custom Pagination for custom post type(s)
 * @param ($wp_query) wp_query
 */
function cs_pagination($pages = '', $range = 10) {
	global $paged;
	$showitems = ($range * 2)+1;
	if(empty($paged)) $paged = 1;

	if($pages == ''){
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages){
			$pages = 1;
		}
	}

	if(1 != $pages){
		echo '<div class="pagination-wrapper"><div class="inner-pagination-wrapper"><ul class="pagination">';
		if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo '<li><a href="'.get_pagenum_link(1).'">&laquo; First</a></li>';
		if($paged > 1 && $showitems < $pages) echo '<li><a href="'.get_pagenum_link($paged - 1).'">&lsaquo; Previous</a></li>';
		for($i=1; $i <= $pages; $i++){
			if(1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
				echo ($paged == $i)? '<li class="active"><span class="current">'.$i.'</span></li>' : '<li><a href="'.get_pagenum_link($i).'" class="inactive">'.$i.'</a></li>';
			}
		}
		if($paged < $pages && $showitems < $pages) echo '<li><a href="'.get_pagenum_link($paged + 1).'">Next &rsaquo;</a></li>';
		if($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo '<li><a href="'.get_pagenum_link($pages).'">Last &raquo;</a></li>';
		echo '</ul></div></div>';
	}
}

/**
 * ------------------------------------
 * FIX - Make taxonomy pagination work!
 * ------------------------------------
 * Solves issue with /page/2/ of taxonomy giving a 404 error
 */
add_filter('request', 'taxonomy_request');
function taxonomy_request($query_string ){
	if( isset( $query_string['page'] ) ) {
		if( ''!=$query_string['page'] ) {
			if( isset( $query_string['name'] ) ) {
				unset( $query_string['name'] );
			}
		}
	}
	return $query_string;
}

add_action('pre_get_posts','taxonomy_pre_get_posts');
function taxonomy_pre_get_posts( $query ){
	if(is_tax() && $query->is_main_query() && !$query->is_feed() && !is_admin()){
		$query->set( 'paged', str_replace( '/', '', get_query_var( 'page' ) ) );
	}
}

/**
 * --------------------------------------------
 * FIX - Make custom post type pagination work!
 * --------------------------------------------
 * Taken from http://wordpress.stackexchange.com/a/16929/9244
 * Solves issue with /page/2/ of custom post types giving a 404 error
 * remove if you're not using custom post type or not paginating
 */
add_action('init', 'wpse16902_init');
function wpse16902_init(){
	$GLOBALS['wp_rewrite']->use_verbose_page_rules = true;
}

add_filter('page_rewrite_rules', 'wpse16902_collect_page_rewrite_rules');
function wpse16902_collect_page_rewrite_rules( $page_rewrite_rules ){
	$GLOBALS['wpse16902_page_rewrite_rules'] = $page_rewrite_rules;
	return array();
}

add_filter('rewrite_rules_array', 'wspe16902_prepend_page_rewrite_rules');
function wspe16902_prepend_page_rewrite_rules( $rewrite_rules ){
	return $GLOBALS['wpse16902_page_rewrite_rules'] + $rewrite_rules;
}

/**
 * -------------
 * Display Error
 * -------------
 * For debugging when using wpdb. 
 */
function show_error(){
	global $wpdb;
	if($wpdb->last_error !== '') :
		$str   = htmlspecialchars( $wpdb->last_result, ENT_QUOTES );
		$query = htmlspecialchars( $wpdb->last_query, ENT_QUOTES );
		echo "<div id='error'>
		<p class='wpdberror'><strong>WordPress database error:</strong> [$str]<br />
		<code>$query</code></p>
		</div>";
	endif;
}


function days_diff($pickup_date, $return_date) {
	if ($pickup_date == $return_date) {
		return 1;
	} else {
		$pickup_date = strtotime($pickup_date); // or your date as well
		$return_date = strtotime($return_date);
		$datediff = $return_date - $pickup_date;

		return round($datediff / (60 * 60 * 24));
	}
}

function show_log($string) {
	echo "<pre>";
	print_r($string);
	echo "</pre>";
}


function session_order_setcookie($session_id) {
	ob_start();

	if (!isset($_COOKIE['cart_session_order'])) {
		setcookie('cart_session_order', $session_id, time()+86400, '/');
	} else {
		$session_id = $_COOKIE['cart_session_order'];
		setcookie('cart_session_order', $session_id, time()+86400, '/');
	}
}
// add_action( 'init', 'session_order_setcookie' );


function base_pagination($max_num_pages) {
	global $wp_query;

	$big = 999999999; // This needs to be an unlikely integer

	// For more options and info view the docs for paginate_links()
	// http://codex.wordpress.org/Function_Reference/paginate_links
	$paginate_links = paginate_links( array(
		'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
		'current' => max( 1, get_query_var('paged') ),
		'total' => $max_num_pages,
		'prev_text' => '<i class="fas fa-angle-double-left"></i>',
		'next_text' => '<i class="fas fa-angle-double-right"></i>',
		// 'mid_size' => 5
	) );

	// Display the pagination if more than one page is found
	if ( $paginate_links ) {
		echo '<div class="pagination justify-content-center">';
		echo $paginate_links;
		echo '</div><!--// end .pagination -->';
	}
}


function reading_time($content) {
	$average_word_per_minute 	= 300; 	// average words per minute
	$min_reading_time 			= 1; 	// minimum minute

	$content_arr = explode(' ', trim(strip_tags(get_the_content())));

	$readting_time = round((count($content_arr) > $average_word_per_minute) ? count($content_arr)/$average_word_per_minute : $min_reading_time);

	$label = (($readting_time > 1) ? ' minutes' : ' minute').' read';

	return $readting_time . $label;
}


function short_description($id = null, $content = null, $max = 300) {
	if($id == null && $content == null) {
		$output = get_the_excerpt($id);
	} else {
		if(!has_excerpt($id)) {
			$output = substr(trim( strip_tags( strip_shortcodes($content) ) ), 0, $max);
		} else {
			$output = get_the_excerpt($id);
		}
	}

	return $output.'...';
}


function wp_get_attachment( $attachment_id ) {

	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}


function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if it is enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list)){
            $return['primary_category'] = $categories_list[0];  //get the first category
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}

function get_post_doctor_detail($post_id = false) {
	if($post_id) {
		$doctors_id = get_post_meta( $post_id, 'cmb2_post_multicheckbox', true);

		if($doctors_id) {
			$doctor_detail = get_doctor_detail($doctors_id);
		}
	}

	return @$doctor_detail;
}


function get_doctor_detail($doctors_id = false) {
	if($doctors_id) {
		$product_args = array(
			'post_type' 	=> 'doctor',
			'post_status'	=> 'publish',
			'p' 			=> $doctors_id
		);
		$get_doctor = new WP_Query($product_args);
	
		$doctor_data = array();
	
		while ( $get_doctor->have_posts() ) { $get_doctor->the_post();
			$doctor_data = array('name' 		=> $get_doctor->post->post_title,
								'avatar' 		=> get_post_meta($get_doctor->post->ID, 'cmb2_avatar', true),
								'tiny_avatar' 	=> get_post_meta($get_doctor->post->ID, 'cmb2_tiny_avatar', true),
								'address'		=> get_post_meta($get_doctor->post->ID, 'cmb2_addres', true),
								'id' 			=> $doctors_id,
								'doctor_link'	=> get_permalink($doctors_id)
			);
		}
		
		$post_categories = get_post_primary_category($doctors_id, 'specialization');
		$specialization = @$post_categories['primary_category'];
	
		$specialization_data = array('specialization' 		=> $specialization->name,
									'specialization_id' 	=> $specialization->term_id,
									'specialization_link'	=> get_term_link($specialization->term_id));
	
		$doctor_detail = array_merge($doctor_data, $specialization_data);
	
		wp_reset_postdata();
	}
	
	return $doctor_detail;
}

// stay here for the meantime, you will be deleted soon! hahaha!
function get_doctor_specialization($post_id) {
	$post_categories = get_post_primary_category($post_id, 'specialization');
	$primary_category = $post_categories['primary_category'];
	
	return $primary_category;
}