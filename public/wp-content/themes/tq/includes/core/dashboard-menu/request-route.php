<?php

/**
 * -------------------- 
 * CL - Order List
 * --------------------
 * Custom Admin page for Order List
 */

function reg_menu_orders(){
	$orders_page = add_menu_page(
		'Request Quote',
		'Request Quote',
		'manage_options',
		'product-order',
		'render_template',
		'dashicons-admin-users',
		9
	);
	add_action( 'load-' . $orders_page, 'orders_enqueue' );

	$order_submenu_page = add_submenu_page( 'product-order', 'Email Setting', 'Email Setting', 'manage_options', 'email-setting', 'email_setting_callback' ); 
	add_action( 'load-' . $order_submenu_page, 'orders_enqueue' );
}
add_action('admin_menu', 'reg_menu_orders');

function render_template(){
	get_template_part('template-parts/backend/template-orders');
}

function email_setting_callback(){
	get_template_part('template-parts/backend/email-settings');
}

function orders_enqueue(){
	// These file(s) are only included in inquiry page
	// CSS
	wp_enqueue_style( 'css-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), '', 'all' );
	wp_enqueue_style( 'css-datatable', 'https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css', array(), '', 'all' );
	wp_enqueue_style( 'css-datatable-button', 'https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css', array(), '', 'all' );
	wp_enqueue_style( 'css-custom', get_stylesheet_directory_uri() . '/dist/css/style-admin-customer-list.css', array(), '', 'all' );
	// JS
	wp_enqueue_script('js-jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', FALSE, '', TRUE);
	wp_enqueue_script('js-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', FALSE, '', TRUE);
	wp_enqueue_script('js-datatable', 'https://cdn.datatables.net/v/bs/dt-1.10.13/datatables.min.js', FALSE, '', TRUE);
	wp_enqueue_script('js-datatable-button', 'https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js', FALSE, '', TRUE);
	wp_enqueue_script('js-jszip', '//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js', FALSE, '', TRUE);
	wp_enqueue_script('js-pdfmake', 'https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js', FALSE, '', TRUE);
	wp_enqueue_script('js-vfs_fonts', 'https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js', FALSE, '', TRUE);
	wp_enqueue_script('js-buttons_html5', 'https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js', FALSE, '', TRUE);
	wp_enqueue_script('js-buttons_print', 'https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js', FALSE, '', TRUE);
	wp_enqueue_script('js-admin', get_stylesheet_directory_uri() . '/dist/js/script-admin.js', FALSE, '1.0.1', TRUE);
	
	// Declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
	wp_localize_script('js-admin', 'wpAjax', array(
		'ajaxUrl'   => admin_url('admin-ajax.php'),
		'ajaxNonce' => wp_create_nonce('wp_nonce')
	));
}

/**
 * ----------------------
 * Admin - Get Orders
 * ----------------------
 * Get list of orders
 */
add_action('wp_ajax_get_orders', 'get_orders');
function get_orders(){
	global $wpdb;
	$table = $wpdb->prefix.'orders';

	// $source     = $_POST['source'];
	$columns    = ['first_name', 'last_name', 'order_read', 'email', 'order_id', 'date_created']; // Set columns for retrieving data
	$col_search = ['first_name', 'last_name', 'email', 'order_id']; // Set column for datatable searchable

	$res = render_dt_data($table, $columns, $col_search);
	wp_send_json($res);
	wp_die();
}


/**
 * ----------------------
 * Admin - Get Order Information
 * ----------------------
 * Show Order Information
 */

add_action('wp_ajax_getRequestQuote', 'getRequestQuote');
function getRequestQuote() {
	$post_data = $_POST;

	global $wpdb;

	try {
		/***************************************
		 * Verify serial number if still available
		 */
		$table = $wpdb->prefix.'orders';
		$get_order_detail = $wpdb->get_row("SELECT * 
												FROM $table
												WHERE order_id = '".$post_data['order_id']."'
												");
		if($get_order_detail) {
			$data['status'] = 1;

			$data['data']['order_read'] = $get_order_detail->order_read;
			$data['data']['session_key'] = $get_order_detail->session_key;
			$data['data']['name'] = $get_order_detail->first_name . ' ' .$get_order_detail->last_name;
			$data['data']['address'] = $get_order_detail->street_address;
			$data['data']['email'] = $get_order_detail->email;
			$data['data']['phone'] = $get_order_detail->phone;
			$data['data']['company_name'] = $get_order_detail->company_name;
			$data['data']['street_address'] = $get_order_detail->street_address;

			if(!empty($get_order_detail->order)) {
				$product_list = '';
				$product_cart = unserialize($get_order_detail->order);
				foreach ($product_cart['product_cart'] as $key => $product) {
					$prod_detail = get_post($product['product_id']);
					$post_thumbnail_id 	= get_post_thumbnail_id( $product['product_id'] );
					$thumbnail_thumb 	= wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
					$thumbnail_thumb 	= ($thumbnail_thumb[0] != '') ? $thumbnail_thumb[0] : '';

					$product_list .= '<tr>
										<td>
											<a href="'.get_permalink($prod_detail->ID).'"><img src="'.$thumbnail_thumb.'" alt="" width="50"></a>
										</td>
										<td>
											<a href="'.get_permalink($prod_detail->ID).'">'.$prod_detail->post_title.'</a>
										</td>
										<td>
											'.$product['quantity'].'
										</td>
									</tr>';
				}

				$product_list_html = '<table class="table">
										<tr>
											<th>Image</th>
											<th>Product Name</th>
											<th>Quantity</th>
										</tr>
										'.$product_list.'
									</table>';
			}
			$data['data']['product_list_html'] = $product_list_html;

			$serialize_data 	= serialize($data);
			$result 			= $serialize_data;
			throw new Exception($result);
		} else {
			$data['status']	 = 0;
			$data['message'] = 'There an error occurred while processing your request. Please try again later';

			$result = serialize($data);
			throw new Exception($result);
		}
	} catch (Exception $e) {
		$result = unserialize($e->getMessage());
		if($result['message']) {
			$result['message'] = '<div class="alert alert-danger" role="alert"><p>'.$result['message'].'</p></div>';
		}
		wp_send_json($result);
		wp_die();
	}
}