<?php 

function update_recipients() {
	$post_data = $_POST;

	global $wpdb;

	try {

		$result['success'] = true;

		if($post_data['recipients'] == '') {
			throw new Exception();
		} else {
			$recipients_array = array();
			$get_recipients = explode(',', $post_data['recipients']);
			foreach ($get_recipients as $key => $recipient_email) {
				if (filter_var(trim($recipient_email), FILTER_VALIDATE_EMAIL)) {
					array_push($recipients_array, trim($recipient_email));
				}
			}
			if(!empty($recipients_array)) {
				$recipients = implode(',', $recipients_array);
			} else {
				throw new Exception();
			}
		}

		$table = $wpdb->prefix.'order_settings';

		$update_recipients = $wpdb->update( 
									$table, 
									array( 
										'value' => $recipients,	// accepted
									), 
									array( 'name' => 'recipients' ), 
									array( 
										'%s'	// string
									), 
									array( '%s' ) 
								);

		if($update_recipients === false) {
			throw new Exception();
		} else {
			$result['message'] = 'Recipients has been updated';
		}
		
	} catch (Exception $e) {
		$result['success'] = false;

		if($e->getMessage() != '') {
			$result['message'] = $e->getMessage();
		}
	}
	
	wp_send_json($result);
	wp_die();
}
add_action('wp_ajax_update_recipients', 'update_recipients');