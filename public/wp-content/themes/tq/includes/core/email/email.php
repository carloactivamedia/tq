<?php

// add_action( 'phpmailer_init', 'wpse8170_phpmailer_init' );
function wpse8170_phpmailer_init( PHPMailer $phpmailer ) {
	$phpmailer->isSMTP();
	$phpmailer->Host = '';
	$phpmailer->Port = 465; // could be different
	$phpmailer->Username = ''; // if required
	$phpmailer->Password = ''; // if required
	$phpmailer->SMTPAuth = true; // if required
	$phpmailer->SMTPSecure = 'ssl'; // enable if required, 'tls' is another possible value
}


function do_send_email($recipient = 'carlo@amph.com.ph', $cc = null, $bcc = null, $from = 'GS <carlo@amph.com.ph>', $subject = 'Email Subject', $message_content = null, $reply_to = '') {
	$subject 	= $subject;
	$to 		= $recipient;
	$cc 		= $cc;
	$bcc 		= $bcc;

	$header 	= "from: ".$from." \r\n";
	$header 	.= "MIME-Version: 1.0 \r\n";
	$header 	.= "Content-Type: text/html; charset=ISO-8859-1 \r\n";
	if($cc != null) {
		$header 	.= "Cc: $cc \r\n";
	}
	if($bcc != null) {
		$header 	.= "Bcc: $bcc \r\n";
	}
	
	if($reply_to != null && $reply_to != '') {
		$header 	.= "Reply-To: $reply_to \r\n";
	}

	// Generate HTML email template
	$message_template = $message_content;
	
	// Email sending
	$send_email = wp_mail($to, $subject, $message_template, $header);

	if($send_email) {
		return true;
	} else {
		return false;
	}
}


function generage_email_template($order_id = null, $header_text = '', $email_message = '') {
	try {
		global $wpdb;

		// $order_id = 443;

		if(empty($order_id)) {
			throw new Exception();
		}

		$orders_detail = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."orders WHERE order_id = ".$order_id );

		if(!$orders_detail) {
			throw new Exception();
		}

		$cart_items = unserialize($orders_detail->order);
		$product_list = '';
		foreach ($cart_items['product_cart'] as $key => $product) {
			$prod_detail = get_post($product['product_id']);
			
			$post_thumbnail_id 	= get_post_thumbnail_id( $product['product_id'] );
			$thumbnail_thumb 	= wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
			$thumbnail_thumb 	= ($thumbnail_thumb[0] != '') ? $thumbnail_thumb[0] : '';
			$product_list .= '<tr class="order_item">
								<td class="td" style="text-align: left; vertical-align: middle; border: 1px solid #eee; word-wrap: break-word; color: #636363; padding: 12px;">
									<a href="'.get_permalink($prod_detail->ID).'"><img src="'.$thumbnail_thumb.'" alt="" width="50"></a>
									<a href="'.get_permalink($prod_detail->ID).'">'.$prod_detail->post_title.'</a>
								</td>
								<td class="td" style="text-align: left; vertical-align: middle; border: 1px solid #eee; color: #636363; padding: 12px;"><span class="woocommerce-Price-amount amount">
									'.$product['quantity'].'
								</td>
							</tr>';
		}

		$email_template = '<html>
								<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
									<div id="wrapper" dir="ltr" style="background-color: #f7f7f7; margin: 0; padding: 70px 0 70px 0; -webkit-text-size-adjust: none !important; width: 100%;">
										<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
											<tr>
												<td align="center" valign="top">
													<div id="template_header_image"> </div>
													<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important; background-color: #ffffff; border: 1px solid #dedede; border-radius: 3px !important;">
														<tr>
															<td align="center" valign="top">
																<!-- Header -->
																<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style="background-color: #fff; border-radius: 3px 3px 0 0 !important; color: #ffffff; border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;">
																	<tr>
																		<td id="header_wrapper" style="padding: 36px 48px; display: block; margin: 0 auto; width: 100%;">
																			<img alt="Ezzi Living" src="'.get_bloginfo('template_url').'/dist/images/logo.png">
																		</td>
																	</tr>
																</table>
																<!-- End Header -->
															</td>
														</tr>
														<tr>
															<td align="center" valign="top">
																<!-- Body -->
																<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
																	<tr>
																		<td valign="top" id="body_content" style="background-color: #ffffff;">
																			<!-- Content -->
																			<table border="0" cellpadding="20" cellspacing="0" width="100%">
																				<tr>
																					<td valign="top" style="padding: 48px;">
																						<div id="body_content_inner" style="color: #636363; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
																							<p style="margin: 0 0 16px;">'.$email_message.'</p>
																							<h2 style="color: #1a82ca; display: block; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; margin: 16px 0 8px; text-align: left;"> <span style="font-weight:normal">Request #'.$order_id.'</span> (<time datetime="2018-06-03T05:01:41+00:00">'.date('F j, Y', strtotime($orders_detail->date_created)).'</time>)</h2>
																							<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif; color: #636363; border: 1px solid #e5e5e5;" border="1">
																								<thead>
																									<tr>
																										<th class="td" scope="col" style="text-align: left; color: #636363; border: 1px solid #e5e5e5; padding: 12px;">Product</th>
																										<th class="td" scope="col" style="text-align: left; color: #636363; border: 1px solid #e5e5e5; padding: 12px;">Quantity</th>
																									</tr>
																								</thead>
																								<tbody>
																									'.$product_list.'
																								</tbody>
																							</table>
																							<h2 style="color: #1a82ca; display: block; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; margin: 16px 0 8px; text-align: left;">Customer details</h2>
																							<ul>
																							<li> <strong>Name:</strong> <span class="text" style="color: #3c3c3c; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;">'.$orders_detail->first_name.' '.$orders_detail->last_name.'</span> </li>
																							<li> <strong>Email address:</strong> <span class="text" style="color: #3c3c3c; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;">'.$orders_detail->email.'</span> </li>
																							<li> <strong>Phone:</strong> <span class="text" style="color: #3c3c3c; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;">'.$orders_detail->phone.'</span> </li>
																							<li> <strong>Address:</strong> <span class="text" style="color: #3c3c3c; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;">'.$orders_detail->street_address.'</span> </li>
																							<li> <strong>Company Name:</strong> <span class="text" style="color: #3c3c3c; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;">'.$orders_detail->company_name.'</span> </li>
																							</ul>
																						</div>
																					</td>
																				</tr>
																			</table>
																			<!-- End Content -->
																		</td>
																	</tr>
																</table>
																<!-- End Body -->
															</td>
														</tr>
														<tr>
															<td align="center" valign="top">
																<!-- Footer -->
																<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
																	<tr>
																		<td valign="top" style="padding: 0; -webkit-border-radius: 6px;">
																			<table border="0" cellpadding="10" cellspacing="0" width="100%">
																				<tr>
																					<td colspan="2" valign="middle" id="credit" style="padding: 0 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #c09bb9; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;">
																						<p>Ezzi Living</p>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
																<!-- End Footer -->
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
								</body>
								</html>';

		return $email_template;

	} catch (Exception $e) {
		return false;
	}
}