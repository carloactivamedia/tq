<?php

function cmb2_doctors_panel_fields() {
	$prefix = 'cmb2_';

	/**
	 * Product Details
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'doctor_panel_detail',
		'title'         => __( 'Doctor Information', 'cmb2' ),
		'object_types'  => array( 'doctor' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Avatar', 'cmb2' ),
		'desc' => esc_html__( 'Preferred Size : 270 x 180', 'cmb2' ),
		'id'   => $prefix . 'avatar',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		)
	) );
	
	$cmb->add_field( array(
		'name' => esc_html__( 'Tiny Thumbnail', 'cmb2' ),
		'desc' => esc_html__( 'Preferred Size : 80 x 80', 'cmb2' ),
		'id'   => $prefix . 'tiny_avatar',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		)
	) );

	$cmb->add_field( array(
		'name'    => esc_html__( 'Clinic name & Address', 'cmb2' ),
		'id'      => $prefix.'addres',
		'type'    => 'wysiwyg',
		'options' => array(
			'wpautop' => true, // use wpautop?
			'media_buttons' => false, // show insert/upload button(s)
			'textarea_rows' => get_option('default_post_edit_rows', 10),
			'teeny' => false, // output the minimal editor config used in Press This
			'tinymce' => true
		),
	) );


		
	
}
add_action( 'cmb2_admin_init', 'cmb2_doctors_panel_fields' );