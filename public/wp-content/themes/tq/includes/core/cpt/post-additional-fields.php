<?php
function cmb2_post_fields() {
	$prefix = 'cmb2_';

	/**
	 * Post Meta Box
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'post_fields',
		'title'         => __( 'Page Banner' ),
		'object_types'  => array( 'post', 'specialist_ofd_month' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Banner' ),
		'id'   => $prefix . 'page_banner',
		'type' => 'file',
        'options' => array(
			'url' => false, // Hide the text input for the url
		)
	) );

	/**
	 * Post Meta Box
	 */
	$cmb2 = new_cmb2_box( array(
		'id'            => 'featured_post_fields',
		'title'         => __( 'Featured Sider' ),
		'object_types'  => array( 'post' ),
		'context'       => 'side',
		'priority'      => 'high',
		'show_names'    => true
	) );

	$cmb2->add_field( array(
		'name' => esc_html__( 'Feature on homepage slider' ),
		'id'   => $prefix . 'featured_slider',
		'type' => 'checkbox'
	) );


	/**
	 * Doctor Meta Box
	 */
	$cmb3 = new_cmb2_box( array(
		'id'            => 'specialized_doctors',
		'title'         => __( 'Specialized Doctor' ),
		'object_types'  => array( 'post', 'specialist_ofd_month' ),
		'context'       => 'side',
		'priority'      => 'high',
		'show_names'    => true
	) );

	$cmb3->add_field( array(
		// 'name'       => __( 'Select Doctor', 'cmb2' ),
		// 'desc'       => __( 'field description (optional)', 'cmb2' ),
		'id'         => $prefix . 'post_multicheckbox',
		'type'       => 'radio',
		'options_cb' => 'cmb2_get_your_post_type_post_options',
	) );

	/**
	 * Gets a number of posts and displays them as options
	 * @param  array $query_args Optional. Overrides defaults.
	 * @return array             An array of options that matches the CMB2 options array
	 */
	function cmb2_get_post_options( $query_args ) {

		$args = wp_parse_args( $query_args, array(
			'post_type'   => 'post',
			'numberposts' => 10,
		) );

		$posts = get_posts( $args );

		$post_options = array();
		if ( $posts ) {
			foreach ( $posts as $post ) {
				$post_options[ $post->ID ] = $post->post_title;
			}
		}

		return $post_options;
	}

	/**
	 * Gets 5 posts for your_post_type and displays them as options
	 * @return array An array of options that matches the CMB2 options array
	 */
	function cmb2_get_your_post_type_post_options() {
		return cmb2_get_post_options( array( 'post_type' => 'doctor', 'numberposts' => -1 ) );
	}
}
add_action( 'cmb2_admin_init', 'cmb2_post_fields' );