<?php

function cmb2_specialization_fields() {
	$prefix = 'cmb2_';

	/**
	 * Product Details
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'specialization_detail',
		'title'         => __( 'Specialization', 'cmb2' ),
		'object_types'     => array( 'term' ),
		'taxonomies'       => array( 'specialization' )
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Thumbnail', 'cmb2' ),
		// 'desc' => esc_html__( 'Preferred Size : 270 x 180', 'cmb2' ),
		'id'   => $prefix . 'specialization_thumbnail',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		)
	) );

	// $cmb->add_field( array(
	// 	'name'    => esc_html__( 'Specialization Content', 'cmb2' ),
	// 	'id'      => $prefix.'specialization_content',
	// 	'type'    => 'wysiwyg',
	// 	'options' => array(
	// 		'wpautop' => true, // use wpautop?
	// 		'media_buttons' => false, // show insert/upload button(s)
	// 		'textarea_rows' => get_option('default_post_edit_rows', 10),
	// 		'teeny' => false, // output the minimal editor config used in Press This
	// 		'tinymce' => true
	// 	),
	// ) );

	
}
add_action( 'cmb2_admin_init', 'cmb2_specialization_fields' );