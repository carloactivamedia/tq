<?php

function cmb2_ads_manager_fields() {
	$prefix = 'cmb2_';

	/**
	 * Snippet / Code Box
	 */
	$cmb3 = new_cmb2_box( array(
		'id'            => 'ads_fields',
		'title'         => __( 'Snippet / Code' ),
		'object_types'  => array( 'ads_manager' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => false
	) );

	$cmb3->add_field( array(
		'name' => esc_html__( 'Snippet / Code' ),
		'id'   => $prefix . 'ad_snippet',
		'type' => 'textarea_code'
	) );
	
}
add_action( 'cmb2_admin_init', 'cmb2_ads_manager_fields' );