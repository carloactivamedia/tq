<?php

function cmb2_category() {
	$prefix = 'cmb2_';

	/**
	 * Product Details
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'category_fields',
		'title'         => __( 'Category Sub Title' ),
		'object_types'     => array( 'term' ),
		'taxonomies'       => array( 'category' )
	) );

	$cmb->add_field( array(
		'name' => esc_html__( 'Thumbnail', 'cmb2' ),
		// 'desc' => esc_html__( 'Preferred Size : 270 x 180', 'cmb2' ),
		'id'   => $prefix . 'thumbnail',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		)
	) );

	$cmb->add_field( array(
		'name'    => esc_html__( 'Sub Title' ),
		'id'      => $prefix.'subtitle',
		'type'    => 'wysiwyg',
		'options' => array(
			'wpautop' => true, // use wpautop?
			'media_buttons' => false, // show insert/upload button(s)
			'textarea_rows' => get_option('default_post_edit_rows', 10),
			'teeny' => false, // output the minimal editor config used in Press This
			'tinymce' => true
		)
	) );
}
add_action( 'cmb2_admin_init', 'cmb2_category' );