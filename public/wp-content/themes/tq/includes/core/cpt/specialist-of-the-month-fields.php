<?php

function cmb2_specialist_of_the_month_fields() {
	$prefix = 'cmb2_';

	/**
	 * Overlay Content Box
	 */
	$cmb3 = new_cmb2_box( array(
		'id'            => 'overlay_content_fields',
		'title'         => __( 'Overlay Content' ),
		'object_types'  => array( 'specialist_ofd_month' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => false
	) );

	$cmb3->add_field( array(
		'name' => esc_html__( 'Overlay Content' ),
		'id'   => $prefix . 'overlay_content',
		'type' => 'wysiwyg',
        'options' => array(
			'wpautop' => true,
			'textarea_rows' => get_option('default_post_edit_rows', 10) // rows="..."
		)
	) );

	/**
	 * Specialist of the Month Box
	 */
	$cmb2 = new_cmb2_box( array(
		'id'            => 'specialist_post_fields',
		'title'         => __( 'Specialist of the Month' ),
		'object_types'  => array( 'specialist_ofd_month' ),
		'context'       => 'side',
		'priority'      => 'high',
		'show_names'    => true
	) );

	$cmb2->add_field( array(
		'name' => esc_html__( 'Feature as Specialist of the Month' ),
		'id'   => $prefix . 'specialist_of_the_month',
		'type' => 'checkbox'
	) );
	
}
add_action( 'cmb2_admin_init', 'cmb2_specialist_of_the_month_fields' );