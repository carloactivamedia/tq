<?php /* Template Name: Medical Concierge */ ?>
<?php get_header(); ?>

<div id="content-wrapper">

	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>
	
	<div class="page-section">
			
		<div class="header-banner-wrapper">
			<div class="container">
				<div class="header-banner">
					<h3 class="category-title">
						This Quarterly
					</h3>
					<h1 class="post-title"><?php echo get_the_title() ?></h1>
				</div>
			</div>
		</div>

		<div class="container">

			<div class="post-content">
				<?php echo wpautop(get_the_content()) ?>
				
				<div class="row">
					<div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
						<!-- feedback.activamedia.com.sg script begins here --><script type="text/javascript" defer src="//feedback.activamedia.com.sg/embed/4479547.js" data-role="form" data-default-width="650px"></script><!-- feedback.activamedia.com.sg script ends here -->
					</div>
				</div>
			</div>
			
		</div>

		<?php //echo get_template_part('template-parts/other-articles') ?>
		
	</div>

</div>

<?php get_footer(); ?>