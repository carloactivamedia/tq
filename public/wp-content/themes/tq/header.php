<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo get_bloginfo('template_url') ?>/dist/images/favicon.ico" type="image/x-icon" />
	
	<title><?php wp_title(''); ?></title>

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

	<?php wp_head(); ?>

	<style type="text/css">

		@font-face {
			font-family: 'Open Sans';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans.svg#OpenSans') format('svg');
			font-weight: normal;
			font-style: normal;
		}

		@font-face {
			font-family: 'Open Sans Light';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Light.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Light.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Light.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Light.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Light.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Light.svg#OpenSans') format('svg');
			font-weight: normal;
			font-style: normal;
		}
		
		@font-face {
			font-family: 'Open Sans Semibold';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Semibold.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Semibold.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Semibold.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Semibold.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Semibold.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/opensans/OpenSans-Semibold.svg#OpenSans-Semibold') format('svg');
			font-weight: 600;
			font-style: normal;
		}

		@font-face {
			font-family: 'Gotham Medium';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Medium.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Medium.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Medium.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Medium.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Medium.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Medium.svg#Gotham-Medium') format('svg');
			font-weight: 500;
			font-style: normal;
		}

		@font-face {
			font-family: 'Sanford';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/sanford/Sanford-Book.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/sanford/Sanford-Book.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/sanford/Sanford-Book.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/sanford/Sanford-Book.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/sanford/Sanford-Book.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/sanford/Sanford-Book.svg#Sanford-Book') format('svg');
			font-weight: normal;
			font-style: italic;
		}

		@font-face {
			font-family: 'Gotham Bold';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Bold.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Bold.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Bold.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Bold.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Bold.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Bold.svg#Gotham-Bold') format('svg');
			font-weight: bold;
			font-style: normal;
		}

		@font-face {
			font-family: 'Gotham Book';
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Book.eot');
			src: url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Book.eot?#iefix') format('embedded-opentype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Book.woff2') format('woff2'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Book.woff') format('woff'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Book.ttf') format('truetype'),
				url('<?php echo get_bloginfo('template_url') ?>/dist/fonts/gotham/Gotham-Book.svg#Gotham-Book') format('svg');
			font-weight: 500;
			font-style: normal;
		}

	</style>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110881685-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-110881685-1');
	</script>

</head>

<body <?php body_class(); ?>>

<!-- HEADER -->
<div id="header-wrapper">

	<div class="sticky-header">
		<div class="top-header">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-12 col-12 order-lg-2">
						<div class="section addddd-section white-bg">
							<div class="section-title">ADVERTISEMENT</div>
							<a href="https://www.montblanc.com/en/home.html" target="_blank"><img src="<?php echo get_bloginfo('template_url') ?>/dist/images/addddd-mont-blanc.jpg" alt="" class="img-fluid"></a>
						</div>
						<div class="clearfix"></div>
						<div class="current-date">
							<?php echo date('l, j M Y'); ?>
						</div>
					</div>
					<div class="col-lg-4 col-md-12 col-12 order-lg-1">
						<div class="header-logo-wrapper">
							<div class="header-logo">
								<a href="<?php echo get_bloginfo('url') ?>">
									<img src="<?php echo get_bloginfo('template_url') ?>/dist/images/logo.png" class="img-fluid" alt="">
								</a>
							</div>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<i class="fas fa-bars"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="bottom-header">
			<div class="container">
				<nav class="navbar navbar-expand-lg navbar-light">
					<a class="navbar-brand" href="<?php echo get_bloginfo('url') ?>">
						<img src="<?php echo get_bloginfo('template_url') ?>/dist/images/logo-tiny.png" class="img-fluid" alt="">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-bars"></i>
					</button>
					<div id="navbarSupportedContent" class="collapse navbar-collapse">
						<?php 
						if ( has_nav_menu( 'primary' ) ) :
							wp_nav_menu( array(
								'theme_location'    => 'primary',
								'depth'             => 3,
								'container'         => '',
								'menu_class'        => 'navbar-nav',
								'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
								'walker'            => new WP_Bootstrap_Navwalker(),
							) );
						endif;
						?>
						<ul id="second-menu" class="navbar-nav ml-auto">
							<li class="nav-item form-container">
								<form class="form-inline my-2" action="<?php echo esc_url( home_url( '/' ) ) ?>" role="search" method="get" >
									<div class="input-group">
										<?php
											$unique_id = esc_attr( uniqid( 'search-form-' ) );
											$search_query = get_search_query();
										?>
										<input name="s" type="search" id="<?php echo $unique_id ?>" class="form-control" aria-label="Search..." placeholder="Search..." value="<?php echo $search_query ?>">
										<div class="input-group-append">
											<span class="input-group-text"><i class="fas fa-search"></i></span>
										</div>
									</div>
								</form>
							</li>
							<li>
								<ul class="social-media-links">
									<li class="nav-item">
										<a href="https://www.instagram.com/thisquarterly/" target="_blank" class="nav-link social-media instagram">
											<i class="fab fa-instagram"></i>
										</a>
									</li>
									<li class="nav-item">
										<a href="https://www.facebook.com/thisquarterly/" target="_blank" class="nav-link social-media facebook">
											<i class="fab fa-facebook-f"></i>
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>

			</div>
		</div>
	</div>
	
</div> <!-- header-wrapper -->