<?php get_header(); ?>
<?php $view_mode = (@$_GET['view'] == 'list') ? 'list' : 'grid'; ?>

<div id="content-wrapper">

	<?php if ( function_exists('yoast_breadcrumb') ) { ?>
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php yoast_breadcrumb('<div class="breadcrumbs">','</div>'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="products-section section">

		<div class="container">
			<div class="row">

				<div class="col-lg-12">

					<div class="product-wrapper">

						<div class="toolbar">
							<div class="row">
								<div class="col-lg-6 col-md-6">
									<div class="display-counter">
										<label>
											<?php
												/**
												 * Get the page number of custom taxonomy
												 * Note: somehow get_query_var( 'paged' ) does not work on custom taxonomy
												 * this is for the meatime
												 */
												$current_url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
												$current_url = explode('/', $current_url);
												$target_key = 0;
												$paged = 1;
												$has_pagination = false;

												foreach ($current_url as $key => $slug) {
													if($slug == 'page') {
														$target_key = ++$key;
														$has_pagination = true;
														break;
													}
												}

												if($has_pagination) {
													$paged = $current_url[$target_key];
												}

												$search_qry 	= get_search_query();
												
												$product_counter_qry = array(
													'post_type' 		=> array('specialist_ofd_month', 'post'),
													'post_status'		=> 'publish',
													'post_date' 		=> 'date',
													'order'				=> 'DESC',
													'posts_per_page' 	=> -1
												);

												if($search_qry != '') {
													$product_counter_qry['s'] = $search_qry;
												}

												$product_counter = new wp_query( $product_counter_qry );
												$post_count = $product_counter->post_count;
												$posts_per_page = get_option('posts_per_page');

												if($paged == 1) {
													$showing_first = 1;
													$last = $posts_per_page*$paged;
												} else {
													$showing_first = ($posts_per_page*($paged-1))+1;
													$last = $posts_per_page*$paged;
												}

												if($last > $post_count) {
													$last = $post_count;
												}
											?>
											Showing <?php echo $showing_first."-".$last; ?> of <?php echo $post_count ?> results
										</label>
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="sort-selection float-right">
										<div class="btn-group">
											<button type="button" class="btn btn-link btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<span class="label">Filter By:</span>
												<span class="sort">
													<?php
														switch (@$_GET['sort']) {
															case 'newest':
																echo 'Newest';
																break;
															
															case 'a-z':
																echo 'A-Z';
																break;
															
															case 'z-a':
																echo 'Z-A';
																break;
															
															default:
																echo 'Newest';
																break;
														}
													?>
												</span>
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="<?php echo appendQueryString( array('sort' => 'newest') ) ?>">Newest</a>
												<a class="dropdown-item" href="<?php echo appendQueryString( array('sort' => 'a-z') ) ?>">Name (A-Z)</a>
												<a class="dropdown-item" href="<?php echo appendQueryString( array('sort' => 'z-a') ) ?>">Name (Z-A)</a>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div> <!-- toolbar -->

						<!-- <div class="product-<?php echo $view_mode ?>"> -->
						<div class="section related-posts-section">
							<?php
								/**
								 * Get the page number of custom taxonomy
								 * Note: somehow get_query_var( 'paged' ) does not work on custom taxonomy
								 * this is for the meatime
								 */
								$current_url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
								$current_url = explode('/', $current_url);
								$target_key = 0;
								$paged = 0;
								$has_pagination = false;

								foreach ($current_url as $key => $slug) {
									if($slug == 'page') {
										$target_key = ++$key;
										$has_pagination = true;
										break;
									}
								}

								if($has_pagination) {
									$paged = $current_url[$target_key];
								}

								$search_qry 	= get_search_query();
								
								$product_args = array(
									'post_type' 	=> array('specialist_ofd_month', 'post'),
									'post_status'	=> 'publish',
									'paged' 		=> $paged
								);

								if($search_qry != '') {
									$product_args['s'] = $search_qry;
								}

								switch (@$_GET['sort']) {
									case 'a-z':
										$product_args = array_merge( $product_args, array('orderby' => 'title', 'order' => 'ASC') );
									break;

									case 'z-a':
										$product_args = array_merge( $product_args, array('orderby' => 'title', 'order' => 'DESC') );
									break;

									case 'newest':
										$product_args = array_merge( $product_args, array('orderby' => 'post_date', 'order' => 'DESC') );
										break;
									
									default:
										$product_args = array_merge( $product_args, array('orderby' => 'post_date', 'order' => 'DESC') );
										break;
								}

								$products = new wp_query( $product_args );
							?>	
							<?php if($products->have_posts()) { ?>
								<div class="related-posts">
									<div class="row">
										
										<?php while ( $products->have_posts() ) { ?>
											<?php 
												$products->the_post();
												$post_thumbnail_id  = get_post_thumbnail_id( $products->post->id );
												$thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
												$thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';

												$terms = wp_get_post_terms( $products->post->ID, array( 'category' ) );
											?>
											<div class="col-lg-3 col-md-6 other-post-wrapper">
												<a href="<?php echo get_permalink($products->post->ID) ?>">
													<div class="other-post" style="background-image:url('<?php echo $thumbnail ?>')">
														<div class="reading-time">
														<i class="far fa-clock"></i> <?php echo reading_time($products->post->post_content); ?>
														</div>
														<div class="other-post-detail">
															<div class="other-post-title">
																<?php echo $products->post->post_title; ?>
															</div>
															<div class="other-post-category">
																<?php echo @$terms[0]->name ?>
															</div>
															<div class="other-post-date">
																<i class="far fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', $products->post->ID); ?>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php } ?>
										
									</div>
								</div>
							<?php } else { ?>
								<div class="no-product">
									<div class="text-center">
										<h4>No products to show</h4>
									</div>
								</div>
							<?php } ?>
							
						</div> <!-- product-grid -->

						<?php //base_pagination($products->max_num_pages) ?>
						<?php cs_pagination($products->max_num_pages) ?>

					</div> <!-- product-grid-wrapper -->
				</div>
				
				

			</div>
		</div>

	</div> <!-- section -->

</div> <!-- content-wrapper -->

<?php get_footer(); ?>