<?php $op_ids = [2184, 2171]; ?>
<?php 
    $featured_food_qry = array(
                            'cat'           => 6,
                            'post_type' 	=> 'post',
                            'post_status'	=> 'publish',
                            'posts_per_page' => 2,
                            'post__in' 			=> $op_ids
                        );
    $featured_food = new wp_query( $featured_food_qry );
    if($featured_food->have_posts()) { 
?>
<div class="section food-section">
    <div class="container">
        <div class="section-title strikethrough style-2 mb-5">
            <span>Food</span>
        </div>

        <div class="featured-post">
            <div class="row">
                <?php $ctr = 0; ?>
                <?php while ( $featured_food->have_posts() ) {?>
                    <?php
                        $ctr++;
                        $featured_food->the_post();

                        $thumbnail_id 	= get_post_thumbnail_id( $featured_food->post->ID );
                        $thumbnail 		= wp_get_attachment_image_src($thumbnail_id, 'large');
                        $thumbnail 		= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
                    ?>
                    <div class="col-lg-6 <?php echo ($ctr == 1) ? 'right-bordered' : '' ?>">
                        <div class="post-detail-wrapper ">
                            <div class="post-detail <?php echo ($ctr == 1) ? 'pr-0' : 'pl-0' ?>">
                                <a href="<?php echo get_permalink($featured_food->post->ID) ?>"><div class="post-thumbnail" style="background-image: url('<?php echo $thumbnail; ?>')"></div></a>
                                <div class="post-title">
                                    <a href="<?php echo get_permalink($featured_food->post->ID) ?>"><?php echo $featured_food->post->post_title ?></a>
                                </div>
                                <div class="post-data-attribute">
                                    <ul class="list-inline">
                                        <?php 
                                            $post_categories = get_post_primary_category($featured_food->post->ID, 'category'); 
                                            $primary_category = $post_categories['primary_category'];
                                            if(!empty($primary_category)) {
                                        ?>
                                        <li class="list-inline-item">
                                            <div class="post-category">
                                                <?php echo $primary_category->name; ?>
                                            </div>
                                        </li>
                                        <?php } ?>
                                        <li class="list-inline-item">
                                            <div class="post-date">
                                                <i class="far fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', $featured_food->post->ID); ?>
                                            </div>
                                        </li>
                                        <li class="list-inline-item">
                                            <div class="reading-time">
                                                <i class="far fa-clock"></i> <?php echo reading_time($featured_food->post->post_content); ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-excerpt">
                                    <?php echo substr(trim( strip_tags( $featured_food->post->post_content ) ), 0, 300) ?>
                                </div>

                                <div class="read-more">
                                    <a href="<?php echo get_permalink($featured_food->post->ID) ?>"><span class="go-to-arrow">Read More <i class="far fa-arrow-alt-circle-right"></i></span></a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>
<?php } ?>
<?php wp_reset_postdata() ?>