<?php
    $slider_args = array( 
                        'post_status'		=> 'publish',
                        'posts_per_page'    => -1,
                        'meta_query'        => array(
                                                    array(
                                                        'key'     => 'cmb2_featured_slider',
                                                        'value'   => 'on',
                                                        'compare' => '=',
                                                    )
                                                )
    );

    $slider_qry = new WP_Query( $slider_args );

    if($slider_qry->have_posts()) {
?>
    <div class="section slider-section">
        <div id="homepage-slider" class="owl-carousel owl-theme">
            <?php while ( $slider_qry->have_posts() ) { ?>
                <?php
                    $slider_qry->the_post();
                    $post_thumbnail_id  = get_post_thumbnail_id( $slider_qry->post->ID );
                    $thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
                    $thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';

                    $terms              = wp_get_post_terms( $slider_qry->post->ID, array( 'category' ) );
                    $doctors            = wp_get_post_terms( $slider_qry->post->ID, array( 'doctors' ) );
                ?>
                <div class="item">
                    <div class="slider-item" style="background-image:url('<?php echo $thumbnail ?>')">
                        <div class="loading-bar-container"><div class="loading-bar"></div></div>
                        <a href="<?php echo get_permalink($slider_qry->post->ID) ?>">
                            <div class="slide-content">
                                <div class="post-category">
                                    <?php echo $terms[0]->name ?>
                                </div>
                                <div class="post-title">
                                    <?php echo $slider_qry->post->post_title ?>
                                </div>
                                <div class="post-author">
                                    <?php echo @$doctors->name ?>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="container position-relative">
            <div class="slider-navigation prev">
                <div class="nav-block">
                    <div class="nav-icon">
                        <i class="far fa-arrow-alt-circle-left"></i>
                    </div>
                    <div class="nav-caption">
                        <div class="post-category">
                            
                        </div>
                        <div class="post-title">
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="slider-navigation next">
                <div class="nav-block">
                    <div class="nav-icon">
                        <i class="far fa-arrow-alt-circle-right"></i>
                    </div>
                    <div class="nav-caption">
                        <div class="post-category">
                            
                        </div>
                        <div class="post-title">
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
<?php } ?>
<?php wp_reset_postdata() ?>