<?php
    $featured_travel_post = 2232;
    $args = array( 
        'p'             => $featured_travel_post,
        'post_type'     => 'post',
        'post_status'	=> 'publish'
    );

    $travel_post = new WP_Query( $args ); 
    // show_log($travel_post);
    if($travel_post->have_posts()) { 
?>
<?php while ( $travel_post->have_posts() ) { ?>
    <?php
        $travel_post->the_post();

        $thumbnail_id 	= get_post_thumbnail_id( $travel_post->post->ID );
        $thumbnail 		= wp_get_attachment_image_src($thumbnail_id, 'large');
        $thumbnail 		= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/no-image.jpg';
    ?>
        <div class="section travel-section">
            <div class="container">
                <div class="section-title strikethrough style-2">
                    <span>Travel</span>
                </div>
                <div class="featured-post">
                    <div class="row">

                        <div class="col-lg-6">
                            <a href="<?php echo get_permalink($travel_post->post->ID) ?>"><div class="post-thumbnail" style="background-image: url('<?php echo $thumbnail; ?>')"></div></a>
                        </div>
                        <div class="col-lg-6">
                            <div class="post-detail-wrapper">
                                
                                <div class="post-detail">
                                    <div class="post-title">
                                        <a href="<?php echo get_permalink($travel_post->post->ID) ?>"><?php echo $travel_post->post->post_title ?></a>
                                    </div>
                                    <div class="post-data-attribute">
                                        <ul class="list-inline">
                                            <?php 
                                                $post_categories = get_post_primary_category($travel_post->post->ID, 'category'); 
                                                $primary_category = $post_categories['primary_category'];
                                                if(!empty($primary_category)) {
                                            ?>
                                            <li class="list-inline-item">
                                                <div class="post-category">
                                                <?php echo $primary_category->name; ?>
                                                </div>
                                            </li>
                                            <?php } ?>
                                            <li class="list-inline-item">
                                                <div class="post-date">
                                                    <i class="far fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', $travel_post->post->ID); ?>
                                                </div>
                                            </li>
                                            <li class="list-inline-item">
                                                <div class="reading-time">
                                                    <i class="far fa-clock"></i> <?php echo reading_time($travel_post->post->post_content); ?>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="post-excerpt">
                                        <?php echo substr(strip_tags($travel_post->post->post_content), 0, 300) ?>
                                    </div>

                                    <div class="read-more">
                                        <a href="<?php echo get_permalink($travel_post->post->ID) ?>"><span class="go-to-arrow">Read More <i class="far fa-arrow-alt-circle-right"></i></span></a>
                                    </div>
                                </div>
                                
                                <div class="other-posts">
                                    <div class="row">
                                        <div class="col-lg-6 other-post-wrapper">
                                            <div class="other-post" style="background-image:url('<?php echo wp_get_attachment_url(2248)?> ')"></div>
                                        </div>
                                        <div class="col-lg-6 other-post-wrapper">
                                            <div class="other-post" style="background-image:url('<?php echo wp_get_attachment_url(2242)?> ')"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
<?php wp_reset_postdata() ?>