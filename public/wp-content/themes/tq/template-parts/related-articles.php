<?php
//for use in the loop, list 5 post titles related to first tag on current post
$tags = wp_get_post_tags(get_the_ID());
if ($tags) {
    echo 'Related Posts';
    $first_tag = $tags[0]->term_id;
    $args=array(
    'tag__in' => array($first_tag),
    'post__not_in' => array(get_the_ID()),
    'posts_per_page'=>4,
    'caller_get_posts'=>1
    );
    $my_query = new WP_Query($args);
    if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post(); ?>
    <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
    
    <?php
    endwhile;
    }
    wp_reset_query();
}
?>
<div class="section related-articles-section">
    <div class="container">
        <div class="section-title strikethrough">
            <span>Related Articles</span>
        </div>

        <div class="related-posts specialist-opinions">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12 other-post-wrapper">
                    <a href="#">
                        <div class="other-post" style="background-image:url('<?php echo get_bloginfo('template_url') ?>/dist/images/featured-post.jpg')">
                            <div class="reading-time">
                                <i class="far fa-clock"></i> 3 min read
                            </div>
                            <div class="other-post-detail">
                                <div class="other-post-title">
                                    Miles & Miles
                                </div>
                                <div class="other-post-category">
                                    Food
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-12 other-post-wrapper">
                    <a href="#">
                        <div class="other-post" style="background-image:url('<?php echo get_bloginfo('template_url') ?>/dist/images/featured-post.jpg')">
                            <div class="reading-time">
                                <i class="far fa-clock"></i> 3 min read
                            </div>
                            <div class="other-post-detail">
                                <div class="other-post-title">
                                    The Havana Gangster
                                </div>
                                <div class="other-post-category">
                                    Food
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-12 other-post-wrapper">
                    <div class="other-post" style="background-image:url('<?php echo get_bloginfo('template_url') ?>/dist/images/featured-post.jpg')">
                        <div class="reading-time">
                            <i class="far fa-clock"></i> 3 min read
                        </div>
                        <div class="other-post-detail">
                            <div class="other-post-title">
                                Miles & Miles
                            </div>
                            <div class="other-post-category">
                                Food
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 other-post-wrapper">
                    <div class="other-post" style="background-image:url('<?php echo get_bloginfo('template_url') ?>/dist/images/featured-post.jpg')">
                        <div class="reading-time">
                            <i class="far fa-clock"></i> 3 min read
                        </div>
                        <div class="other-post-detail">
                            <div class="other-post-title">
                                The Havana Gangster
                            </div>
                            <div class="other-post-category">
                                Food
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- specialist-opinions -->
        
    </div>
</div>