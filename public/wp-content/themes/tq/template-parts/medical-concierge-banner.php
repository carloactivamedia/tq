<div class="section banner-section">
    <div class="container">
        
        <div class="medical-doctors-banner">
            <div class="top-section">
                <div class="row">
                    <div class="col-lg-5">
                        <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/banner-logo.png" alt="" class="img-fluid">
                        <div class="tag-line">
                            Need a medical <br>appointment for your <br>health concerns?
                        </div>
                    </div>
                    <div class="col-lg-7 align-self-end">
                        <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/medical-doctors.png" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="bottom-section">
                <div class="row">
                    <div class="col-lg-9">
                        <p>TQ’s panel of specialists comprises established medical and dental professionals who are experienced and respected within the fraternity. Whether it is an ENT, sports injury, gynaecological or skin problem, we aim to help you get an appointment within 1 business day at various private medical centres because we understand your anxiety.</p>
                        <a href="<?php echo get_permalink(273) ?>"><span class="go-to-arrow">Book an appointment now <i class="far fa-arrow-alt-circle-right"></i></span></a>
                    </div>
                    <div class="col-lg-3">
                        <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/desk-bell.png" alt="" class="img-fluid desk-bell">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>