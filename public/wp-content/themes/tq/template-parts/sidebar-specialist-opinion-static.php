<?php $doctor = get_post_doctor_detail(get_the_ID()); ?>
<?php $page_banner = get_post_meta( get_the_ID(), 'cmb2_page_banner', true ); ?>

<?php if($doctor) { ?>
<div class="sidebar doctor <?php echo ($page_banner != '') ? 'offset-top' : '' ?>">
    <div class="sidebar-content">
        <div class="avatar">
            <a href="<?php echo $doctor['doctor_link'] ?>"><img src="<?php echo $doctor['tiny_avatar']; ?>" alt=""></a>
        </div>
        <div class="doctor-name">
            <a href="<?php echo $doctor['doctor_link'] ?>"><strong><?php echo $doctor['name'] ?></strong></a>
        </div>
        <div class="job-title">
            <?php echo $doctor['specialization']; ?>
        </div>
        <div class="address">
            <?php echo wpautop($doctor['address']); ?>
        </div>
    </div>
</div>
<?php } ?>

<div class="sidebar carousel specialist-opinion">
    <div class="sidebar-title">Specialist Opinion <div class="sidebar-navigation-wrapper"><div class="sidebar-navigation specialist prev"><i class="far fa-arrow-alt-circle-left"></i></div> <div class="sidebar-navigation specialist next"><i class="far fa-arrow-alt-circle-right"></i></div></div></div>
    <div class="sidebar-content">

        <div id="specialist-opinion-slider" class="owl-carousel owl-theme">

            <?php //$category = get_queried_object(); ?>
            <?php //$category_id = $category->term_id; ?>
            <?php 
            // show_log($category_id);
                $args = array( 
                        'cat'               => 3,
                        'post_type'         => 'post',
                        'posts_per_page'    => 6,
                        'post_status'	    => 'publish',
                        'orderby'           => 'rand',
                        'post__not_in'      => [get_the_ID()]
                );
        
                $op = new WP_Query( $args ); 
            ?>

            <?php 
                if($op->have_posts()) {
                    $ctr = 0;
                    $ctr_loop = 0;
                    $post_count = $op->post_count;
                    while ( $op->have_posts() ) {
                        $op->the_post();
                        $ctr++;
                        $ctr_loop++;
                        $post_thumbnail_id  = get_post_thumbnail_id( $op->post->ID );
                        $thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
                        $thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';

                        $soa_doctor = get_post_doctor_detail($op->post->ID);
                        if(@$soa_doctor) {
                            $doctor_detail = '<div class="author">
                                                <div class="name">'.$soa_doctor['name'].'</div>
                                                <div class="field-job">'.$soa_doctor['specialization'].'</div>
                                            </div>';
                        }

                        @$op_post .= '<div class="media-object">
                                        <div class="media-thumb" style="background-image:url(\''.$thumbnail.'\')"></div>
                                        <div class="media-detail">
                                            <div class="media-title">
                                                <a href="'.get_permalink($op->post->ID).'">'.$op->post->post_title.'</a>
                                            </div>
                                            '.@$doctor_detail.'
                                        </div>
                                    </div>';
                        if($ctr == 3 || $post_count == $ctr_loop) {
                            @$build_op_post .= '<div class="item"><div class="media-object-list">'.$op_post.'</div></div>';
                            $ctr = 0;
                            $op_post = '';
                        }
                            
                    }
                }
            ?>
            
            <?php echo $build_op_post ?>
        
        </div> <!-- #featured-slider -->

    </div>
</div>

<?php echo get_template_part('template-parts/banners/bmw-alpina-portrait') ?>