<div class="wrap">

	<h1>Email Setting</h1>
	<br/>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5 col-sm-12">
				
				<div class="form-wrapper">

					<div id="form-notification"></div>

					<?php 

					global $wpdb;

					$get_row = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."order_settings WHERE name = 'recipients'");

					if($get_row) {
						$recipients = $get_row->value;
					}

					?>
					
					<form>
						<div class="form-group">
							<label>
								Email Recipient(s)
							</label>
							<textarea class="form-control" name="recipients"><?php echo @$recipients ?></textarea>
							<small>Enter recipients (comma separated)</small>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-md" id="update-recipients">Submit</button>
						</div>
					</form>

				</div>

			</div>
		</div>
	</div>

</div>