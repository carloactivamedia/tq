<?php
    $op_ids = [2194, 2202];
    $wp_post_qry = array(
                            'post_type' 	    => 'post',
                            'post_status'	    => 'publish',
                            'posts_per_page'    => 2,
                            'post__in' 			=> $op_ids
                        );
    $wp_post = new wp_query( $wp_post_qry );
    if($wp_post->have_posts()) { 
?>
<div class="section food-section">
    <div class="container">
        <div class="section-title strikethrough style-2 mb-5">
            <span>Lifestyle</span>
        </div>

        <div class="featured-post">

            <div class="row">
                <?php $ctr = 0; ?>
                <?php while ( $wp_post->have_posts() ) {?>
                    <?php
                        $ctr++;
                        $wp_post->the_post();

                        $thumbnail_id 	= get_post_thumbnail_id( $wp_post->post->ID );
                        $thumbnail 		= wp_get_attachment_image_src($thumbnail_id, 'large');
                        $thumbnail 		= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
                    ?>
                    <div class="col-lg-6 <?php echo ($ctr == 1) ? 'right-bordered' : '' ?>">
                        <div class="post-detail-wrapper <?php echo ($ctr == 1) ? '' : '' ?>">
                            <div class="post-detail pr-0">
                                <a href="<?php echo get_permalink($wp_post->post->ID) ?>"><div class="post-thumbnail" style="background-image: url('<?php echo $thumbnail; ?>')"></div></a>
                                <div class="post-title">
                                    <a href="<?php echo get_permalink($wp_post->post->ID) ?>"><?php echo $wp_post->post->post_title ?></a>
                                </div>
                                <div class="post-data-attribute">
                                    <ul class="list-inline">
                                        <?php 
                                            $post_categories = get_post_primary_category($wp_post->post->ID, 'category'); 
                                            $primary_category = $post_categories['primary_category'];
                                            if(!empty($primary_category)) {
                                        ?>
                                        <li class="list-inline-item">
                                            <div class="post-category">
                                                <?php echo $primary_category->name; ?>
                                            </div>
                                        </li>
                                        <?php } ?>
                                        <li class="list-inline-item">
                                            <div class="post-date">
                                                <i class="far fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', $wp_post->post->ID); ?>
                                            </div>
                                        </li>
                                        <li class="list-inline-item">
                                            <div class="reading-time">
                                                <i class="far fa-clock"></i> <?php echo reading_time($wp_post->post->post_content); ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-excerpt">
                                    <?php echo substr(trim( strip_tags( $wp_post->post->post_content ) ), 0, 300) ?>
                                </div>

                                <div class="read-more">
                                    <a href="<?php echo get_permalink($wp_post->post->ID) ?>"><span class="go-to-arrow">Read More <i class="far fa-arrow-alt-circle-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            
            <?php
                $op_ids = [2189, 1784, 1768, 1781];
                $other_post_qry = array(
                                        'post_type' 	=> 'post',
                                        'post_status'	=> 'publish',
                                        'posts_per_page'=> 4,
                                        'post__in' 		=> $op_ids
                                    );
                $other_post = new wp_query( $other_post_qry );
                if($other_post->have_posts()) { 
            ?>
                <div class="other-posts">
                    <div class="other-posts-caption">
                        Other Lifestyle Articles
                    </div>
                    <div class="row">
                        <?php while ( $other_post->have_posts() ) {?>
                            <?php 
                                $other_post->the_post();

                                $thumbnail_id 	= get_post_thumbnail_id( $other_post->post->ID );
                                $thumbnail 		= wp_get_attachment_image_src($thumbnail_id, 'large');
                                $thumbnail 		= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
                            ?>
                            <div class="col-lg-3 other-post-wrapper">
                                <a href="<?php echo get_permalink($other_post->post->ID) ?>">
                                    <div class="other-post" style="background-image:url('<?php echo $thumbnail; ?>')">
                                        <div class="reading-time">
                                            <i class="far fa-clock"></i> <?php echo reading_time($other_post->post->post_content); ?>
                                        </div>
                                        <div class="other-post-detail">
                                            <div class="other-post-title">
                                                <?php echo $other_post->post->post_title ?>
                                            </div>
                                            <?php 
                                                $post_categories = get_post_primary_category($other_post->post->ID, 'category'); 
                                                $primary_category = $post_categories['primary_category'];
                                                if(!empty($primary_category)) {
                                            ?>
                                            <div class="other-post-category">
                                                <?php echo $primary_category->name; ?>
                                            </div>
                                            <div class="other-post-date">
                                                <i class="far fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', $other_post->post->ID); ?>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</div>
<?php } ?>
<?php wp_reset_postdata() ?>