<?php 
    $testimonials_args = array(
                'post_type' 		=> 'testimonial',
                'post_status'		=> 'publish',
                'posts_per_page'    => 4,
                'post__in' 		=> array(208, 200, 207, 206)
            );
    $testimonials = new wp_query( $testimonials_args );
?>
<?php if($testimonials->have_posts()) { ?>
    <div class="testimonials-section section">
        <div class="container">
            <div class="section-title">
                What our client say:
            </div>

            <div class="row">
                <?php while ( $testimonials->have_posts() ) { $testimonials->the_post(); ?>
                    <div class="col-lg-3 testimonial-col">
                        <div class="testimonial-block">
                            <div class="author">
                                <?php echo get_the_title( $testimonials->post->ID ); ?>
                            </div>

                            <?php $location = get_post_meta(get_the_ID(), 'p_location', true); ?>
                            <?php if($location != '') { ?>
                                <div class="location">
                                    <?php echo $location; ?>
                                </div>
                            <?php } ?>

                            <div class="stars">
                                <?php $stars = get_post_meta(get_the_ID(), 'p_stars', true); ?>
                                <?php for ($i=0; $i < 5; $i++) { ?>
                                    <i class="fa<?php echo ($i >= $stars) ? 'r' : 's' ?> fa-star"></i>
                                <?php } ?>
                            </div>

                            <div class="testimonial">
                                <?php
                                    $adsa = get_the_content( $testimonials->post->ID );
                                    if(strlen(trim(strip_tags($adsa))) >  230) {
                                        $adsa = substr(trim(strip_tags($adsa)), 0, 230).'...';
                                    }
                                ?>
                                <?php echo nl2br($adsa); ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
                
            <div class="button-block">
                <a href="<?php echo get_permalink(203) ?>" class="btn btn-site blue">Read more</a>
            </div>
        </div>
    </div>
    <div class="separator-tail"></div>
<?php } ?>