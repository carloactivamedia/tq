<?php
    $featured_args = array( 
                        'post_type'         => 'specialist_ofd_month',
                        'post_status'		=> 'publish',
                        'posts_per_page'    => 1,
                        'meta_query'        => array(
                                                    array(
                                                        'key'     => 'cmb2_specialist_of_the_month',
                                                        'value'   => 'on',
                                                        'compare' => '=',
                                                    )
                                                )
    );

    $featured_qry = new WP_Query( $featured_args );

    if($featured_qry->have_posts()) {
?>
    <?php while ( $featured_qry->have_posts() ) { ?>
        <?php
            $featured_qry->the_post();

            $thumbnail_id 	= get_post_thumbnail_id( $featured_qry->post->ID );
            $thumbnail 		= wp_get_attachment_image_src($thumbnail_id, 'large');
            $thumbnail 		= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';

            // $doctors = wp_get_post_terms( $featured_qry->post->ID, array( 'doctors' ) );
            $doctor = get_post_doctor_detail($featured_qry->post->ID);
        ?>
        <div class="section featured-blog-post-section">
            <div class="container">
                <div class="featured-blog-post">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="post-thumbnail">
                                <div class="post-thumbmail-img" style="background-image: url('<?php echo $thumbnail ?>')">
                                    <img src="<?php echo $thumbnail ?>" alt="" class="img-fluid">
                                </div>
                                <div class="thumbnail-content-wrapper">
                                    <div class="thumbnail-content">
                                        <?php echo wpautop( get_post_meta($featured_qry->post->ID, 'cmb2_overlay_content', true) ) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="post-detail">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 order-md-2">
                                        <div class="reading-time">
                                            <i class="far fa-clock"></i> <?php echo reading_time($featured_qry->post->post_content); ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 order-md-1">
                                        <?php if(@$doctor['specialization']) { ?>
                                            <div class="post-category">
                                                <?php echo $doctor['specialization']; ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="post-title">
                                    <a href="<?php echo get_permalink($featured_qry->post->ID) ?>"><?php echo $featured_qry->post->post_title ?></a>
                                </div>
                                <?php if($doctor) { ?>
                                    <div class="post-author">
                                        by <?php echo $doctor['name']; ?>
                                    </div>
                                <?php } ?>
                                <div class="post-excerpt">
                                    <?php echo substr( trim( strip_tags(strip_shortcodes($featured_qry->post->post_content)) ), 0, 500).'...' ?>
                                </div>
                                <div class="read-more">
                                    <br>
                                    <a href="<?php echo get_permalink($featured_qry->post->ID) ?>"><span class="go-to-arrow">Read More <i class="far fa-arrow-alt-circle-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
<?php wp_reset_postdata() ?>