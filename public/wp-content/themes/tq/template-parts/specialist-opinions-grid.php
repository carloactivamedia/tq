<?php if(have_posts()) { ?>
    <div class="section related-posts-section">
        <div class="related-posts">
            <div class="row">
                <?php while ( have_posts() ) { the_post(); ?>
                    <div class="col-xl-3 col-lg-4 col-md-6 other-post-wrapper">
                        <?php
                            $post_thumbnail_id  = get_post_thumbnail_id( get_the_ID() );
                            $thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
                            $thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
                        ?>
                        <a href="<?php echo get_permalink(get_the_ID()) ?>">
                            <div class="other-post" style="background-image:url('<?php echo @$thumbnail ?>')">
                                <div class="reading-time">
                                    <i class="far fa-clock"></i> <?php echo reading_time(get_the_content()); ?>
                                </div>
                                <div class="other-post-detail">
                                    <div class="other-post-title">
                                        <?php echo get_the_title() ?>
                                    </div>
                                    <div class="other-post-category">
                                        <?php
                                            $get_category = get_the_category(get_the_ID());
                                            $categor_name = @$get_category[0]->name;
                                            echo $categor_name;
                                        ?>
                                    </div>
                                    <div class="other-post-date">
                                        <i class="far fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', get_the_ID()); ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>