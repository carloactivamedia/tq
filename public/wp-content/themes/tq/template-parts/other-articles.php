<div class="section related-articles-section">
	<div class="container">
		<div class="section-title strikethrough">
			<span>Other Articles</span>
		</div>

		<div class="related-posts specialist-opinions">
			<div class="row">
				<?php
				$get_post_type = get_post_type(get_the_ID());
				if($get_post_type != 'specialist_ofd_month') { // Exclude Specialist of the month when viewing post under Specialist of the month
					$sotm_args = array( 
										'post_type'         => 'specialist_ofd_month',
										'post_status'		=> 'publish',
										'posts_per_page'    => 1,
										'meta_query'        => array(
																	array(
																		'key'     => 'cmb2_specialist_of_the_month',
																		'value'   => 'on',
																		'compare' => '=',
																	)
																)
					);

					$sotm = new WP_Query( $sotm_args );

					if($sotm->have_posts()) {
					?>
					<?php while ( $sotm->have_posts() ) { ?>
						<?php
							$sotm->the_post();

							$thumbnail_id 	= get_post_thumbnail_id( $sotm->post->ID );
							$thumbnail 		= wp_get_attachment_image_src($thumbnail_id, 'large');
							$thumbnail 		= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
						?>
						<div class="col-lg-3 col-md-6 col-12 other-post-wrapper">
							<a href="<?php echo get_permalink($sotm->post->ID) ?>">
								<div class="other-post" style="background-image:url('<?php echo $thumbnail ?>')">
									<div class="reading-time">
										<i class="far fa-clock"></i> <?php echo reading_time($sotm->post->post_content); ?>
									</div>
									<div class="other-post-detail">
										<div class="other-post-title">
											<?php echo $sotm->post->post_title ?>
										</div>
										<div class="other-post-category">
											<?php
												// $post_categories = get_post_primary_category($sotm->post->ID, 'specialization');
												// $specialization = @$post_categories['primary_category'];
												// echo $specialization->name;
											?>
										</div>
									</div>
								</div>
							</a>
						</div>
						<?php } ?>
						<?php wp_reset_postdata() ?>
					<?php } ?>
				<?php } ?>


				<?php 
					/**
					 * 3 = Specialist Opinions
					 * 5 = Travel
					 * 6 = Food
					 * 7 = Lifestyle
					 */
					$post_categories = get_post_primary_category(get_the_ID(), 'category'); 
					$primary_category = @$post_categories['primary_category'];

					$cond = @$primary_category->term_id;

					switch ($cond) {
						case 5:
							$category_sequence = array(6,7,3);
							break;

						case 6:
							$category_sequence = array(5,7,3);
							break;

						case 7:
							$category_sequence = array(5,6,3);
							break;
						
						default:
							$category_sequence = array(5,6,7);
							break;
					}

					if($get_post_type == 'specialist_ofd_month') {
						$category_sequence = array(3,5,6,7);
					}
					
					foreach ($category_sequence as $category_id) {
						$other_article_arg = array( 
											'cat'         		=> $category_id,
											'post_status'		=> 'publish',
											'orderby'           => 'rand',
											'posts_per_page'    => 1
										);

						$other_article = new WP_Query( $other_article_arg );
						if($other_article->have_posts()) {
							while ( $other_article->have_posts() ) {
								$other_article->the_post();
								$thumbnail_id 	= get_post_thumbnail_id( $other_article->post->ID );
								$thumbnail 		= wp_get_attachment_image_src($thumbnail_id, 'large');
								$thumbnail 		= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
				?>

								<div class="col-lg-3 col-md-6 col-12 other-post-wrapper">
									<a href="<?php echo get_permalink($other_article->post->ID) ?>">
										<div class="other-post" style="background-image:url('<?php echo $thumbnail; ?> ')">
											<div class="reading-time">
												<i class="far fa-clock"></i> <?php echo reading_time($other_article->post->post_content); ?>
											</div>
											<div class="other-post-detail">
												<div class="other-post-title">
													<?php echo $other_article->post->post_title; ?>
												</div>
												<div class="other-post-category">
													<?php
														$post_categories = get_post_primary_category($other_article->post->ID, 'category'); 
														$primary_category = $post_categories['primary_category'];
														echo $primary_category->name;
													?>
												</div>
											</div>
										</div>
									</a>
								</div>
						<?php } ?>
						<?php wp_reset_postdata() ?>
					<?php } ?>
				<?php } ?>
				
			</div>
		</div> <!-- specialist-opinions -->
		
	</div>
</div>