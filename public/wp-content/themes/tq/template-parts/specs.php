<div class="specs-section">
    <div class="container">

        <div class="specs-grid">
            <div class="specs">
                <div class="specs-thumb">
                    <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/durable-thumb.png" alt="">
                </div>    
                <div class="specs-name">
                    Durable
                </div>
            </div>
            <div class="specs">
                <div class="specs-thumb">
                    <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/space-saving-thumb.png" alt="">
                </div>    
                <div class="specs-name">
                    Space Saving
                </div>
            </div>
            <div class="specs">
                <div class="specs-thumb">
                    <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/easy-to-use-thumb.png" alt="">
                </div>    
                <div class="specs-name">
                    Easy to Use
                </div>
            </div>
            <div class="specs">
                <div class="specs-thumb">
                    <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/rust-proof-thumb.png" alt="">
                </div>    
                <div class="specs-name">
                    Rust Proof
                </div>
            </div>
            <div class="specs">
                <div class="specs-thumb">
                    <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/practical-thumb.png" alt="">
                </div>    
                <div class="specs-name">
                    Practical
                </div>
            </div>
        </div>

    </div>
</div>