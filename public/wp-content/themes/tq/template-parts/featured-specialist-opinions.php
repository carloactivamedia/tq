<div class="section specialist-opinions-section">
    <div class="container">
        <div class="section-title strikethrough">
            <span>Specialist Opinions</span>
        </div>
        <div class="section-subtitle">
            <p>These are the specialist medical practitioners who make THIS Quarterly a credible and authentic healthcare and lifestyle publication. <br>Get updated with the latest healthcare happenings here.</p>
        </div>

        <div class="specialist-opinions">
        
            <?php $op_ids = [2295, 2323, 2334, 2339, 2313, 2099]; ?>
            <?php 
                $featured_op_qry = array(
                                        'cat'           => 3,
                                        'post_type' 	=> 'post',
                                        'post_status'	=> 'publish',
                                        'posts_per_page' => 6,
                                        'post__in' 		=> $op_ids
                                    );
                $featured_op = new wp_query( $featured_op_qry );
            ?>
            <?php if($featured_op->have_posts()) { ?>
                <div class="row">
                    <?php while ( $featured_op->have_posts() ) { ?>
                    
                        <?php 
                            $featured_op->the_post();
                            $post_thumbnail_id  = get_post_thumbnail_id( $featured_op->post->ID );
                            $thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
                            $thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';
                        ?>
                        <div class="col-lg-6 col-md-6 opinion-outer-wrapper">
                            <div class="opinion-wrapper">
                                <div class="opinion">
                                    <a href="<?php echo get_permalink( $featured_op->post->ID ) ?>" class="opinion-thumbnail" style="background-image: url('<?php echo $thumbnail ?>')"></a>
                                    <div class="opinion-detail">
                                        <div class="opinion-title">
                                            <a href="<?php echo get_permalink( $featured_op->post->ID ) ?>"><?php echo $featured_op->post->post_title ?></a>
                                        </div>
                                        <?php $doctor = get_post_doctor_detail($featured_op->post->ID); ?>
                                        <?php if(@$doctor) { ?>
                                            <a href="<?php echo $doctor['doctor_link'] ?>">
                                                <div class="opinion-author">
                                                    <div class="author-thumbnail">
                                                        <img src="<?php echo $doctor['tiny_avatar'] ?>" alt="">
                                                    </div>
                                                    <div class="author-detail">
                                                        <div class="author-name">
                                                            <?php echo $doctor['name']; ?>
                                                        </div>
                                                        <div class="author-position">
                                                            <?php echo $doctor['specialization'] ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php } ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            <?php } ?>

            <div class="read-more text-center">
                <br>
                <a href="<?php echo get_term_link(3) ?>"><span class="go-to-arrow">Read More <i class="far fa-arrow-alt-circle-right"></i></span></a>
            </div>
            
        </div> <!-- specialist-opinions -->
        
    </div>
</div>