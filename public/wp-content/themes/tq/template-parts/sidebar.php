<?php if(@$primary_category->term_id != 3) { ?>
<div class="sidebar ad">
    <div class="sidebar-title">ADVERTISEMENT</div>
    <div class="sidebar-content">
        <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/addddd-mont-blanc-vertical.jpg" alt="" class="img-fluid">
    </div>
</div>
<?php } ?>

<?php $doctors = wp_get_post_terms( get_the_ID(), array( 'doctors' ) ); ?>
<div class="sidebar carousel specialist-opinion">
    <div class="sidebar-title">Specialist Opinion <div class="sidebar-navigation-wrapper"><div class="sidebar-navigation specialist prev"><i class="far fa-arrow-alt-circle-left"></i></div> <div class="sidebar-navigation specialist next"><i class="far fa-arrow-alt-circle-right"></i></div></div></div>
    <div class="sidebar-content">

        <div id="specialist-opinion-slider" class="owl-carousel owl-theme">

            <?php 
                $args = array( 
                        'cat'               => 3,
                        'post_type'         => 'post',
                        'posts_per_page'    => 6,
                        'post_status'	    => 'publish',
                        'orderby'           => 'rand',
                        'post__not_in'      => [get_the_ID()]
                );
        
                $op = new WP_Query( $args ); 
            ?>

            <?php 
                if($op->have_posts()) {
                    $ctr = 0;
                    $ctr_loop = 0;
                    $post_count = $op->post_count;
                    while ( $op->have_posts() ) {
                        $op->the_post();
                        $ctr++;
                        $ctr_loop++;
                        $post_thumbnail_id  = get_post_thumbnail_id( $op->post->ID );
                        $thumbnail 			= wp_get_attachment_image_src($post_thumbnail_id, 'large');
                        $thumbnail 			= ($thumbnail[0] != '') ? $thumbnail[0] : get_bloginfo('template_url').'/dist/images/default-photo.jpg';

                        $doctors = wp_get_post_terms( $op->post->ID, array( 'doctors' ) );
                        $specialization = get_term_by('slug', get_term_meta($doctors[0]->term_id, 'cmb2_specialization', true), 'specialization');
                        if($doctors) {
                            $doctor_detail = '<div class="author">
                                                <div class="name">'.$doctors[0]->name.'</div>
                                                <div class="field-job">'.$specialization->name.'</div>
                                            </div>';
                        }

                        @$op_post .= '<div class="media-object">
                                        <div class="media-thumb" style="background-image:url(\''.$thumbnail.'\')"></div>
                                        <div class="media-detail">
                                            <div class="media-title">
                                                <a href="'.get_permalink($op->post->ID).'">'.$op->post->post_title.'</a>
                                            </div>
                                            '.@$doctor_detail.'
                                        </div>
                                    </div>';
                        if($ctr == 3 || $post_count == $ctr_loop) {
                            @$build_op_post .= '<div class="item"><div class="media-object-list">'.$op_post.'</div></div>';
                            $ctr = 0;
                            $op_post = '';
                        }
                            
                    }
                }
            ?>
            
            <?php echo $build_op_post ?>
        
        </div> <!-- #featured-slider -->

    </div>
</div>

<div class="sidebar ad">
    <div class="sidebar-title">ADVERTISEMENT</div>
    <div class="sidebar-content">
        <img src="<?php echo get_bloginfo('template_url') ?>/dist/images/addddd-maserati-vertical.jpg" alt="" class="img-fluid">
    </div>
</div>