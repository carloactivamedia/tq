<?php 
	global $wp;
	$current_url = home_url(add_query_arg(array(),$wp->request));
?>
<div class="share-buttons">
	<ul>
		<li>
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_url ?>" class="share-link facebook" target="_blank">
				<div class="social-icon">
					<i class="fab fa-facebook-f"></i>
				</div>
				<span>Facebook</span>
			</a>
		</li>
		<li>
			<a href="http://twitter.com/intent/tweet?text=<?php echo get_the_title() ?>&url=<?php echo $current_url ?>" class="share-link twitter" target="_blank">
				<div class="social-icon">
					<i class="fab fa-twitter"></i>
				</div>
				<span>Twitter</span>
			</a>
		</li>
		<li>
			<a href="https://plus.google.com/share?url=<?php echo $current_url ?>" class="share-link google-plus" target="_blank">
				<div class="social-icon">
					<i class="fab fa-google-plus-g"></i>
				</div>
				<span>Google+</span>
			</a>
		</li>
	</ul>
</div>