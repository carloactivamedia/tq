// const path                    = require('path');
// const webpack                 = require('webpack');
// const ExtractTextPlugin       = require("extract-text-webpack-plugin");
// const UglifyJSPlugin          = require('uglifyjs-webpack-plugin');
// const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// const BrowserSyncPlugin       = require('browser-sync-webpack-plugin');
// const LiveReloadPlugin 		  = require('webpack-livereload-plugin');

// const theme_folder            = 'tq';
// const theme_dir               = './public/wp-content/themes/'+theme_folder;
// const entry_dir               = './src/js/';

// const config = {
// 	entry: {
// 		// app: theme_dir+'/src/js/app.js',
// 		app: './app.js'
// 	},
// 	output: {
// 		filename: './dist/js/[name].min.js',
// 		path: path.resolve(__dirname, theme_dir),
// 	},
// 	module: {
// 		rules: [
// 			{
// 				test: /\.scss$/,
// 				use: ExtractTextPlugin.extract({
// 					fallback: 'style-loader',
// 				  	// use: ['css-loader', 'sass-loader']
// 				  	use: ['css-loader', 'postcss-loader', 'sass-loader']
// 				}),
// 			},
// 			{
// 				test: /\.js$/,
// 				exclude: /(node_modules|bower_components)/,
// 				loader: 'babel-loader',
// 				query: {
// 					presets: ['es2015']
// 				}
// 			}
// 		]
// 	},
// 	plugins: [
// 		// new ExtractTextPlugin('/css/[name].css'),
// 		new ExtractTextPlugin('./style.css'),
// 		new BrowserSyncPlugin({
// 			host: 'localhost',
// 			proxy: 'local.tq',
// 			port: 3038,
// 			// server: { baseDir: ['public'] },
// 			open: false,
// 		    ghostMode: {
// 		        clicks: false,
// 		        location: false,
// 		        forms: false,
// 		        scroll: false
// 			},
// 			files: [
// 		        '**/*.php'
// 		    ],
// 		    injectChanges: true,
// 		    logFileChanges: true,
// 		    logLevel: 'debug',
// 		    logPrefix: 'wepback',
// 		    notify: true,
// 			reloadDelay: 0
// 		},{
// 			// reload: false
// 		}),
// 		// new LiveReloadPlugin({
// 		// 	hostname: 'local.tq',
// 		// 	port: 3038
// 		// }),
// 		new webpack.ProvidePlugin({
//             $: "jquery",
//             jQuery: "jquery",
//             "window.jQuery": "jquery"
//         })
// 	]
// };

// //If true JS and CSS files will be minified
// if (process.env.NODE_ENV === 'production') {
// 	config.plugins.push(
// 		new UglifyJSPlugin(),
// 		new OptimizeCssAssetsPlugin()
// 	);
// }

// module.exports = config;


module.exports = {
	module: {
		rules: [
		{
			test: /\.js$/,
			exclude: /node_modules/,
			use: {
			loader: "babel-loader"
			}
		}
		]
	}
};